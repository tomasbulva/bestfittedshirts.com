<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->
	<?php
	$newsletter = get_field('show_newsletter_signup_form', 'option');
	?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="tier1">
			<div class="container">
					<div class="col-md-<?php echo ($newsletter)? "3" : "4" ?> col-sm-12 col-xs-12">
						<h5>About</h5>
						<div class="tx-div small invert"></div>
						<p>
							<?php echo get_bloginfo( 'description' ); ?>
						</p>
						<div class="tx-div small invert"></div>
						<div class="social-networks">
							<ul class="social-icons">
							<?php 
								$facebook = get_field('facebook', 'option'); 
								$twitter = get_field('twitter', 'option'); 
								$instagram = get_field('instagram', 'option'); 
								$google_plus = get_field('google_plus', 'option'); 
								$pinterest = get_field('pinterest', 'option'); 
							
								if($facebook){
									?>
									<li><a href="<?php echo $facebook; ?>" class="icon icon_facebook"><i class="fa fa-facebook"></i></a></li>
									<?php
								}
								if($twitter){
									?>
									<li><a href="<?php echo $twitter; ?>" class="icon icon_twitter"><i class="fa fa-twitter"></i></a></li>
									<?php
								}
								if($instagram){
									?>
									<li><a href="<?php echo $instagram; ?>" class="icon icon_facebook"><i class="fa fa-instagram"></i></a></li>
									<?php
								}
								if($google_plus){
									?>
									<li><a href="<?php echo $google_plus; ?>" class="icon icon_googleplus"><i class="fa fa-google-plus"></i></a></li>
									<?php
								}
								if($pinterest){
									?>
									<li><a href="<?php echo $pinterest; ?>" class="icon icon_pintrest"><i class="fa fa-pinterest"></i></a></li>
									<?php
								}
							?>

							</ul>
						</div>
					</div>
					<div class="col-md-<?php echo ($newsletter)? "3" : "4" ?> col-sm-12 col-xs-12">
						<h5>Latest</h5>
						<div class="tx-div small invert"></div>
						<ul class="latest-posts">
							<?php wp_reset_query();
							$args2 = array(
								'numberposts' => 3,
								'post_type'   => 'shirt',
								'post_status' => 'publish',
							);
							$shirt_loop = get_posts($args2);
							for($j=0;$j<count($shirt_loop);$j++){
						    	$rawdate = get_the_date( "j M", $shirt_loop[$j]->ID );
						    	$rawdate = trim($rawdate);
						    	$datearr = explode(" ", $rawdate);
						    	$mydate = "<span class='post-date-day'>".$datearr[0]."</span><span class='post-date-month'>".$datearr[1]."</span>";
						    	$title = $shirt_loop[$j]->post_title;
						    	$stars = get_field('stars', $shirt_loop[$j]->ID); //strlen()
						    	$args = array(
									'post_id' => $shirt_loop[$j]->ID,
								);
								// The Query
								$comments_query = new WP_Comment_Query;
								$comments = $comments_query->query( $args );
								// Comment Loop
								if ( $comments ) {
									$commentsnumbers = count($comments);
								} else {
									$commentsnumbers = false;
								}
								echo "<li class='post post-".$j."'>\n";
									echo "<div class='wrapper'>\n";
										echo "<div class='row'>\n";
											echo "<div class='col-lg-2'>\n";
												echo "<div class='date date-square'>";
													echo $mydate;
												echo "</div>\n";
											echo "</div>\n";
											echo "<div class='col-lg-10'>\n";
									    		echo "<a href='".get_post_permalink( $shirt_loop[$j]->ID )."'>".$title."</a>\n";
									    		echo "<div class='post_comments'>\n";
										    		echo "<a href=". get_comments_link( $shirt_loop[$j]->ID ) .">";
													    echo ($commentsnumbers) ? $commentsnumbers . " Comments" : "Leave a comment";
													echo "</a>\n";
												echo "</div>\n";
								    		echo "</div>\n";
							    		echo "</div>\n";
						    		echo "</div>\n";
								echo "</li>\n\n\n";
					        }
							?>
						</ul>
					</div>
					<div class="col-md-<?php echo ($newsletter)? "3" : "4" ?> col-sm-12 col-xs-12">
						<h5>Tags</h5>
						<div class="tx-div small invert"></div>
						<?php $args = array(
								'smallest'                  => 14, 
								'largest'                   => 14,
								'unit'                      => 'px', 
								'number'                    => 45,  
								'format'                    => 'array',
								'separator'                 => "\n",
								'orderby'                   => 'name', 
								'order'                     => 'ASC',
								'exclude'                   => null, 
								'include'                   => null, 
								'topic_count_text_callback' => default_topic_count_text,
								'link'                      => 'view', 
								'taxonomy'                  => 'post_tag', 
								'echo'                      => true,
								'child_of'                  => null, // see Note!
							); ?>
						<?php 
						$cloudtags = wp_tag_cloud( $args );
						foreach( $cloudtags as $tag ){
							 echo $tag . " ";
						}
						?>
					</div>
					<?php if($newsletter){ ?>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<h5>Newsletter</h5>
						<div class="tx-div small invert"></div>
						<div id="mc_embed_signup" class="row">
							<div id="mce-responses" class="clear">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="display:none"></div>
							</div>
							<form action="//bestfittedshirts.us12.list-manage.com/subscribe/post?u=045157293aa0b0d02831016cb&amp;id=2ea1ad0950" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								<label class="col-lg-12"><?php echo get_field('signup_form_label', 'option'); ?>
									<input type="email" value="" name="EMAIL" placeholder="Your E-mail *" class="required email col-lg-12" id="mce-EMAIL">
								</label>
								<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_045157293aa0b0d02831016cb_2ea1ad0950" tabindex="-1" value=""></div>
								<div class="clear col-lg-12"><br><input type="submit" value="<?php echo get_field('button_label', 'option'); ?>" name="subscribe" id="mc-embedded-subscribe" class="btn btn-danger"></div>
							</form>
						</div>
					</div>
					<?php } ?>
			</div>
		</div>
		<div class="tier2">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
					       	<?php
					            $secondary_menu = array(
									'theme_location'    => 'secondary-nav',
									'menu_class'        => 'footer-menu',
									'echo'              => true,
									//'depth'             => 2,
									'container'         => 'div',
									'container_class'   => '',
									'container_id'      => 'footernav',
									'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
									'walker'            =>  new wp_bootstrap_navwalker(),
					            );
					            if(get_field('show_menu_in_footer', 'option')){
					            	wp_nav_menu( $secondary_menu );
								}
					        ?>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12 copyright dark">
						<?php echo get_field('copyright', 'option') ?>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- .site-footer -->

</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>
