<?php
/*
Template name: Shirt of the Week
*/

get_header();
?>

<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<?php wp_reset_query();

					$args_sticky = array(
						'numberposts'			=> 1,
						'post_type'				=> 'shirt',
						'orderby'          		=> 'date',
						'order'            		=> 'DESC',
						'meta_query'			=> array(
							array(
								'key'	  		=> 'deal_of_the_week',
								'value'	  		=> 1,
								'compare' 		=> '==',
							),
						),
						'post_status'			=> 'publish',
					);
					$shirt_sticky_loop = get_posts($args_sticky);
			    	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $shirt_sticky_loop[0]->ID ), 'single-post-thumbnail' );
			    	
			    	$background_image = get_field('background_image', $shirt_sticky_loop[0]->ID);
					$color = get_field('foreground_color',$shirt_sticky_loop[0]->ID);
			    	$background = (get_field('image_as_background', $shirt_sticky_loop[0]->ID)) ? "background-image: url(".$background_image."); background-size:cover; background-position: top center;" : "background-color: ".get_field('color',$shirt_sticky_loop[0]->ID).";";
					$foreground = ($color)? " style='color:".$color."'" : "";
			    	
			    	$title = $shirt_sticky_loop[0]->post_title;
			    	$excerpt = $shirt_sticky_loop[0]->post_excerpt;
			    	$content = $shirt_sticky_loop[0]->post_content;

					echo "<div class='deal_of_the_week_box' style='height:300px; ".$background."'>";
						echo "<div class='container'>";
							echo "<div class='top_banner row col-lg-12'>";
								echo "<p".$foreground." class='alt-font'>Deal Of The Week</p>\n";
					    		echo "<h1".$foreground.">".$title."</h1>\n";
						    	echo "<a href='".$wbuy."' title='Buy here - opens new window' target='_blank' class='btn btn-transparent' style='border-color:".$color."; color:".$color.";'>Learn more</a>\n";
						    echo "</div>\n";
						echo "</div> <!-- / container --> \n\n\n";
					echo "</div>\n";
				?>
			</div> <!-- .container -->
		</main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>