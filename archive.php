<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); 

	if(!$wp_query) global $wp_query;
	$post_type = $wp_query->query_vars['post_type'];

	switch($post_type){
		case "shirt":
			get_template_part('archive-partial-shirt');
			break;
		default:
			get_template_part('archive-partial-default');
			break;
	}

get_footer(); 

?>
