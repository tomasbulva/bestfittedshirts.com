<?php
/*
Template name: Homepage
*/

get_header();

wp_reset_query();

$args = array(
	'numberposts'			=> 1,
	'post_type'				=> 'shirt',
	'post_status'			=> 'publish',
	'meta_query'			=> array(
		array(
			'key'	  		=> 'deal_of_the_week',
			'value'	  		=> 1,
			'compare' 		=> '==',
		),
	),
);

$carouselPosts = get_posts($args);

if(!get_field('full_width_carousel', 'option')){
	?>
		<div class="container">
	<?php
}
?>

	<div class="col-xs-12">
	    <div id="carousel-homepage" class="carousel slide" > <!-- data-ride="carousel" -->
	      <ol class="carousel-indicators">
	      	<li data-target='#carousel-homepage' data-slide-to='0' class='active'></li>
	      	<li data-target='#carousel-homepage' data-slide-to='1'></li>
	      	<li data-target='#carousel-homepage' data-slide-to='2'></li>
	      </ol>
	      <div class="carousel-inner" role="listbox">
	        <?php
	        //
	        // =======> Slide 1 <======= \\
	        //
	        
	        	
	        	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $carouselPosts[0]->ID ), 'single-post-thumbnail' );

	        	$title = $carouselPosts[0]->post_title;
	        	$excerpt = $carouselPosts[0]->post_excerpt;
	        	$content = $carouselPosts[0]->post_content;

	        	$price = get_field('price', $carouselPosts[0]->ID);
	        	$fits = get_the_category($carouselPosts[0]->ID); //get_field('fit', $carouselPosts[0]->ID);
	        	$fitsLastName = end($fits)->name;
	        	$carousel_call_to_action_label = get_field('carousel_call_to_action_label', 'option');
	        	$carousel_button_url_destination = get_field('carousel_button_url_destination', 'option');
	        	$wbuy =  (!$carousel_button_url_destination) ? get_field('where_to_buy_url', $carouselPosts[0]->ID) : get_permalink($carouselPosts[0]->ID);
	        	$stars = get_field('stars', $carouselPosts[0]->ID); //strlen()
	        	$picstars = "";
	        	for ($i2=0;$i2<5;$i2++){
				    		$picstars .= ($i2 < $stars) ? "<i class='glyphicon glyphicon-star'></i>" : "<i class='glyphicon glyphicon-star-empty'></i>";
				}
		    	
		    	$background_image = get_field('background_image', $carouselPosts[0]->ID);
				$color = get_field('color',$carouselPosts[0]->ID);
				$background = (get_field('image_as_background', $carouselPosts[0]->ID)) ? "background-image: url(".$background_image.");" : "background-color: ".$color.";" ;
		        $foreground_color = (get_field('foreground_color',$carouselPosts[0]->ID))? " style='color:".get_field('foreground_color',$carouselPosts[0]->ID)."'" : "";

		        echo "<div class='item slide1 active' style='height:600px; ".$background."'>\n";
		        //echo "<div class='item active' style='height:600px; background-image: url(".$background_image.");'>\n";
		        	echo "<img src='".esc_url( get_template_directory_uri() )."/img/spacer.png' width='100%' height='600' alt='background image'>\n";
			        echo "<div class='carousel-caption'>\n";
				        echo "<div class='col-md-offset-1 col-md-4'>\n";
				        	if(!get_field('image_as_background', $carouselPosts[0]->ID)){
				        		echo "<img src='".$image[0]."' alt='".$title."' class='img-responsive img-thumbnail' style=''>\n";
				        	}
				        echo "</div>\n";
				        echo "<div class='col-md-6'>\n";
				    		echo "<h1".$foreground_color.">".$title."</h1>\n";
				    		echo "<p class='product_meta'>\n";
				    			echo "<span class='fit'>\n";
					    			foreach($fits as $fit){
						    			echo ($fit->name == $fitsLastName) ? $fit->name . "\n" : $fit->name . ", \n";
						    		}
					    		echo "</span>\n";
					    	echo "</p>\n";
				    		echo "<div class='tx-div small invert'".$foreground_color."></div>\n";
				    		echo "<p class='price large'".$foreground_color.">$".$price."</p>\n";
				    		echo "<p".$foreground_color.">".$excerpt."</p>\n";
				    		echo "<p class='carouselStars'>".$picstars."</p>\n";
					    	echo "<a href='".$wbuy."' title='".$carousel_call_to_action_label;
					    	echo (!$carousel_button_url_destination) ? " - opens new window'" : "'";
					    	echo (!$carousel_button_url_destination) ? "target='_blank'" : "";
					    	echo " class='btn btn-primary'>".$carousel_call_to_action_label."</a>\n";
				    	echo "</div>\n";
		  			echo "</div>\n";
		        echo "</div>\n\n\n";
		     
		    //
	        // =======> Slide 2 <======= \\
	        //
	        wp_reset_query();

			$args2 = array(
				'numberposts' => -1,
				'post_type'   => 'shirt',
				'post_status' => 'publish',
			);

			$carouselPosts2 = get_posts($args2);
			//print_r($carouselPosts2);

			$background_image = get_field('background_image', $carouselPosts2[0]->ID);
			$color = get_field('color',$carouselPosts2[0]->ID);
			$background = (get_field('image_as_background', $carouselPosts2[0]->ID)) ? "background-image: url(".$background_image.");" : "background-color: ".$color.";" ;
	        //".$background."
	        echo "<div class='item slide2' style='height:600px;'>\n";
		        echo "<img src='".esc_url( get_template_directory_uri() )."/img/spacer.png' width='100%' height='600' alt='background image'>\n";
			    echo "<div class='carousel-caption'>\n";

			    $counter = (count($carouselPosts2) < 3)? count($carouselPosts2) : 3;
				for($j=0;$j< $counter;$j++){
					
					if(!get_field('deal_of_the_week', $shirt_loop[$j]->ID)){
				    	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $carouselPosts2[$j]->ID ), 'single-post-thumbnail' );

				    	$title = $carouselPosts2[$j]->post_title;
				    	$excerpt = $carouselPosts2[$j]->post_excerpt;
				    	$content = $carouselPosts2[$j]->post_content;

				    	$price = get_field('price', $carouselPosts2[$j]->ID);
				    	$fits = get_the_category($carouselPosts2[$j]->ID); //get_field('fit', $carouselPosts[0]->ID);
				    	$fitsLastName = end($fits)->name;
				    	$carousel_call_to_action_label = get_field('carousel_call_to_action_label', 'option');
			        	$carousel_button_url_destination = get_field('carousel_button_url_destination', 'option');
			        	$wbuy =  (!$carousel_button_url_destination) ? get_field('where_to_buy_url', $carouselPosts[0]->ID) : get_permalink($carouselPosts[0]->ID);
				    	$stars = get_field('stars', $carouselPosts2[$j]->ID); //strlen()
				    	$picstars = "";
				    	for ($i2=0;$i2<5;$i2++){
				    		$picstars .= ($i2 < $stars) ? "<i class='glyphicon glyphicon-star'></i>" : "<i class='glyphicon glyphicon-star-empty'></i>";
				    	}
			        	
			        	echo "<div class='col-md-4 box box-".$j."'>";
			        		echo "<a href='".$wbuy."' title='".$carousel_call_to_action_label;
					    	echo (!$carousel_button_url_destination) ? " - opens new window'" : "'";
					    	echo (!$carousel_button_url_destination) ? "target='_blank'" : "";
					    	echo "class='boxlink'>\n";
					        echo "<img src='".$image[0]."' alt='".$title."' class='img-responsive' style='height:250px;width:auto;'>\n";
				    		echo "<p class='fit'>\n";
				    		foreach($fits as $fit){
						    	echo ($fit->name == $fitsLastName) ? $fit->name . "\n" : $fit->name . ", \n";
						    }
						    echo "</p>\n";
						    echo "<div class='tx-div small center'></div>\n";
				    		echo "<h1 class='product'>".$title."</h1>\n";
				    		//echo "<div class='tx-div small center'></div>\n";
				    		echo "<span class='info-box'>\n";
					    		echo "<p class='stars'>".$picstars."</p>\n";
					    		echo "<p>$".$price."</p>\n";
					    	echo "</span>\n";
					    	echo "<span class='btn btn-primary btn-box'>".$carousel_call_to_action_label."</span></a>\n";
						echo "</div>";
					}

		        }
	        	echo "</div>\n";
		        echo "</div>\n\n\n";

			//
		    // =======> Slide 3 <======= \\
		    //
		    wp_reset_query();

			$args3 = array(
				'numberposts'			=> 1,
				'post_type'				=> 'page',
				'post_status'			=> 'publish',
				'pagename' 				=> 'sloppy-vs-style',
			);

			$carouselPosts3 = get_posts($args3);

			//print_r($carouselPosts3);

			$title = $carouselPosts3[0]->post_title;
	    	$excerpt = $carouselPosts3[0]->post_excerpt;
	    	$content = $carouselPosts3[0]->post_content;

			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $carouselPosts3[0]->ID ), 'single-post-thumbnail' );
			//$color = get_field('color',$carouselPosts3[0]->ID);
			//$background = (get_field('image_as_background', $carouselPosts3[0]->ID)) ? "background-image: url(".$image[0].");" : "background-color: ".$color.";" ;
	        echo "<div class='item slide3' style='height:600px; background-image: url(".$image[0].")'>\n";
		        echo "<img src='".esc_url( get_template_directory_uri() )."/img/spacer.png' width='100%' height='600' alt='background image'>\n";
			        echo "<div class='col-md-offset-1 col-md-9 carousel-caption'>\n";
			    		echo "<h1>".$title."</h1>\n";
			    		echo "<p>".$excerpt."</p>\n";
				    	//echo "<a href='".$wbuy."' title='Buy here - opens new window' target='_blank'>Buy here</a>\n";
		  			echo "</div>\n";
	        echo "</div>\n\n\n";
			?>
	      </div>
	      <a class="left carousel-control" href="#carousel-homepage" role="button" data-slide="prev">
	        <span class="fa fa-chevron-circle-left glyphicon-chevron-left" aria-hidden="true"></span>
	        <span class="sr-only">Previous</span>
	      </a>
	      <a class="right carousel-control" href="#carousel-homepage" role="button" data-slide="next">
	        <span class="fa fa-chevron-circle-right glyphicon-chevron-right" aria-hidden="true"></span>
	        <span class="sr-only">Next</span>
	      </a>
	    </div>
	</div>

<?php
if(!get_field('full_width_carousel', 'option')){
	?>
		</div>
	<?php
}
?>
<div class="container">
	<div class="col-xs-12 secondRow">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
        	<div class="row">
        		<div class="col-md-4">
          			<p class="homepage-icon">
          				<span class="fa-stack fa-3x">
          					<i class="fa fa-circle fa-stack-2x"></i>
          					<i class="fa <?php echo get_field('icon1', 46); ?> fa-stack-1x fa-inverse"></i>
          				</span>
          			</p>
          		</div>
	        	<div class="col-md-8">
          			<?php echo get_field('highlight_column_copy_1', 46); ?>
          			<p><a class="btn btn-default" href="<?php echo get_field('highlight_column_page_1', 46); ?>" role="button">View details »</a></p>
          		</div>
          	</div>
        </div>
        <div class="col-md-4">
	        <div class="row">
        		<div class="col-md-4">
      				<p class="homepage-icon">
      					<span class="fa-stack fa-3x">
          					<i class="fa fa-circle fa-stack-2x"></i>
          					<i class="fa <?php echo get_field('icon2', 46); ?> fa-stack-1x fa-inverse"></i>
          				</span>
      				</p>
      			</div>
	        	<div class="col-md-8">
      				<?php echo get_field('highlight_column_copy_2', 46); ?>
      				<p><a class="btn btn-default" href="<?php echo get_field('highlight_column_page_2', 46); ?>" role="button">View details »</a></p>
      			</div>
          	</div>
       </div>
        <div class="col-md-4">
        	<div class="row">
        		<div class="col-md-4">
					<p class="homepage-icon">
						<span class="fa-stack fa-3x">
          					<i class="fa fa-circle fa-stack-2x"></i>
          					<i class="fa <?php echo get_field('icon3', 46); ?> fa-stack-1x fa-inverse"></i>
          				</span>
					</p>
		        </div>
		        <div class="col-md-8">
		        	<?php echo get_field('highlight_column_copy_3', 46); ?>
		      		<p><a class="btn btn-default" href="<?php echo get_field('highlight_column_page_3', 46); ?>" role="button">View details »</a></p>
	        	</div>
          	</div>
        </div>
      </div>
    </div>
</div>

<?php
get_footer();
?>