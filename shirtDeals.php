<?php
/*
Template name: Shirt Deals
*/

get_header();
?>

<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
			<?php wp_reset_query();

				$args_sticky = array(
					'numberposts'			=> 1,
					'post_type'				=> 'shirt',
					'orderby'          		=> 'date',
					'order'            		=> 'DESC',
					'meta_query'			=> array(
						array(
							'key'	  		=> 'deal_of_the_week',
							'value'	  		=> 1,
							'compare' 		=> '==',
						),
					),
					'post_status'			=> 'publish',
				);
				$shirt_sticky_loop = get_posts($args_sticky);
		    	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $shirt_sticky_loop[0]->ID ), 'single-post-thumbnail' );
		    	
		    	$background_image = get_field('background_image', $shirt_sticky_loop[0]->ID);
				$color = get_field('foreground_color',$shirt_sticky_loop[0]->ID);
		    	$background = (get_field('image_as_background', $shirt_sticky_loop[0]->ID)) ? "background-image: url(".$background_image."); background-size:cover; background-position: top center;" : "background-color: ".get_field('color',$shirt_sticky_loop[0]->ID).";";
				$foreground = ($color)? " style='color:".$color."'" : "";
		    	
		    	$title = $shirt_sticky_loop[0]->post_title;
		    	$excerpt = $shirt_sticky_loop[0]->post_excerpt;
		    	$content = $shirt_sticky_loop[0]->post_content;

				echo "<div class='deal_of_the_week_box' style='height:300px; ".$background."'>";
					echo "<div class='container'>";
						echo "<div class='top_banner row col-lg-12'>";
							echo "<p".$foreground." class='alt-font'>Deal Of The Week</p>\n";
				    		echo "<h1".$foreground.">".$title."</h1>\n";
					    	echo "<a href='".$wbuy."' title='Buy here - opens new window' target='_blank' class='btn btn-transparent' style='border-color:".$color."; color:".$color.";'>Learn more</a>\n";
					    echo "</div>\n";
					echo "</div> <!-- / container --> \n\n\n";
				echo "</div>\n";
			


				$paged = (get_query_var('paged')) ? intval(get_query_var('paged')) : 1;
  				$itemsperpage = 12;

  				wp_reset_query();
				$args2 = array(
					'posts_per_page' => $itemsperpage,
				  	'paged' => $paged,
				  	'orderby' => 'date',
					'order' => 'DESC',
					'numberposts' => -1,
					'post_type'   => 'shirt',
					'post_status' => 'publish',
				);
				$shirt_loop = get_posts($args2);
				$postCount =  wp_count_posts( 'shirt' )->publish; //count($shirt_loop);
				
				$PageStarts = ( ($paged*$itemsperpage) - $itemsperpage )+1;
				$PageEnds = ( ($paged*$itemsperpage) > $postCount )? $postCount : ($paged*$itemsperpage);

				$pagingLeft = ($paged > 1) ? $paged - 1 : false;
				$pagingRight = ($paged < (ceil($postCount / $itemsperpage)) ) ? $paged + 1 : false;
			?>
			<div class="shirt_deals">
				<div class="container">
					<div class="controllerLine row">
						<div class="col-lg-6 left">
							<?php 
			                    if ( function_exists('yoast_breadcrumb') ) {
			                        echo bootstrapStyleBreadCrumbs(yoast_breadcrumb('','',false));
			                    } 
			                ?>
						</div>
						<div class="col-lg-6 right">
							<div class="wrapper">
								<?php 
									echo "Showing ". $PageStarts ."&mdash;". $PageEnds . " of " . $postCount;
								?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="mainColumn col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="row mainContent">
							<?php 

								for($j=0;$j<count($shirt_loop);$j++){
									//if(!get_field('deal_of_the_week', $shirt_loop[$j]->ID)){
								    	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $shirt_loop[$j]->ID ), 'single-post-thumbnail' );
								    	$title = $shirt_loop[$j]->post_title;
								    	$excerpt = $shirt_loop[$j]->post_excerpt;
								    	$content = $shirt_loop[$j]->post_content;
								    	$price = get_field('price', $shirt_loop[$j]->ID);
								    	$fits = get_the_category($shirt_loop[$j]->ID);
								    	$fitsLastName = end($fits)->name;
								    	$wbuy = get_field('where_to_buy_url', $shirt_loop[$j]->ID);
								    	$stars = get_field('stars', $shirt_loop[$j]->ID); //strlen()

								    	$rawdate = get_the_date( "j M", $shirt_loop[$j]->ID );
								    	$rawdate = trim($rawdate);
								    	$datearr = explode(" ", $rawdate);
								    	$mydate = "<span class='post-date-day'>".$datearr[0]."</span><span class='post-date-month'>".$datearr[1]."</span>";

								    	$picstars = "";
								    	$postlink = get_permalink($shirt_loop[$j]->ID);
								    	for ($i2=0;$i2<5;$i2++){
										    $picstars .= ($i2 < $stars) ? "<i class='glyphicon glyphicon-star'></i>" : "<i class='glyphicon glyphicon-star-empty'></i>";
										}
								    	?>
										<div class='col-md-6 col-lg-4 col-sm-12 col-xs-12 productBox productBox-<?php echo $j; ?>'>
											<div class="inner-wrap">
												<div class="like date-square">
													<?php echo get_simple_likes_button( $shirt_loop[$j]->ID ); ?>
												</div>
												<div class="comment date-square">
													<?php echo get_simple_comments_button( $shirt_loop[$j]->ID ); ?>
												</div>
										        <a href="<?php echo $postlink; ?>" class="product-image" style="background-image: url(<?php echo $image[0]; ?>)">
										            <!-- <div > -->
										                <img src='<?php echo esc_url( get_template_directory_uri() ); ?>/img/spacer.png' alt='background image'>
										                <div class="quick-view" data-prod="229">Learn More</div>
										            <!-- </div> -->
										            <!-- end product-image -->
										        </a>
										        <div class='date date-square'>
													<?php echo $mydate; ?>
												</div>
										        <div class="info style-grid1">
										            <div class="text-center">
										                <h5 class="category">
										                	<?php
															foreach($fits as $fit){
												    			echo ($fit->name == $fitsLastName) ? $fit->name . "\n" : $fit->name . ", \n";
												    		}
												    		?>
										                </h5>
										                <div class="tx-div small center"></div>
										                <a href="<?php echo $postlink; ?>">
										                    <p class="name"><?php echo $title; ?></p>
										                </a>
										                <div><p><span class="star-rating" title="Rated <?php echo $stars; ?> out of 5"><?php echo $picstars; ?></span></p></div>
										                <span class="price"><span class="amount">$<?php echo number_format($price,2,".",","); ?></span></span>
										            </div>
										        </div><!-- end info -->
										    </div><!-- .inner-wrap -->
										</div><!-- .box -->

							<?php
						        }
							?>
							</div> <!-- .row -->
							<div class="row">
								<nav>
								  <ul class="pagination">
								    
								    <?php 
								    	// pagination
								    	// ===================
								    	// arrow to the left <
								    	echo ($pagingLeft) ? "<li><a href='?paged=". $pagingLeft ."' aria-label='Previous'><i aria-hidden='true' class='fa fa-angle-left'></i></a></li>" : "<li class='disabled'><a href='#' aria-label='Previous'><i aria-hidden='true' class='fa fa-angle-left'></i></a></li>";
								    
								    	//list of pages
										$total = ceil($postCount / $itemsperpage);
								    	$i = 1;
								    	while($i <= $total){
								    		$activepage = ($i == $paged) ?  " class='active'" : "";
								    		echo "<li". $activepage ."><a href='?paged=".$i."'>".$i."</a></li>\n";
								    		$i++;
								    	}
								    	//arrow to the right >
								     	echo ($pagingRight) ? "<li><a href='?paged=". $pagingRight ."' aria-label='Next'><i aria-hidden='true' class='fa fa-angle-right'></i></a></li>" : "<li class='disabled'><a href='#' aria-label='Next'><i aria-hidden='true' class='fa fa-angle-right'></i></a></li>";
								    ?>
								  </ul>
								</nav>
							</div>
						</div> <!-- .mainColumn -->
						<div class="sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
								<?php dynamic_sidebar( 'sidebar-main' ); ?>
							</div><!-- #primary-sidebar -->
						</div> <!-- .sidebar -->
					</div> <!-- .row -->
				</div> <!-- .container -->
			</div>
		</main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>