<?php
	
	//print_r($wp_query->query_vars);

	$paged = (get_query_var('paged')) ? intval(get_query_var('paged')) : 1;
	$itemsperpage = 12;

	wp_reset_query();
	$args2 = array(
	  	'orderby' => 'date',
		'order' => 'DESC',
		'numberposts' => -1,
		'post_type'   => 'shirt',
		'post_status' => 'publish',
	);

	$superArg = array_merge($wp_query->query_vars, $args2);
	
	//print_r($superArg);

	$shirt_loop = get_posts($superArg);
	// $postCount =  wp_count_posts( 'shirt' )->publish; //count($shirt_loop);
	
	// $PageStarts = ( ($paged*$itemsperpage) - $itemsperpage )+1;
	// $PageEnds = ( ($paged*$itemsperpage) > $postCount )? $postCount : ($paged*$itemsperpage);

	// $pagingLeft = ($paged > 1) ? $paged - 1 : false;
	// $pagingRight = ($paged < (ceil($postCount / $itemsperpage)) ) ? $paged + 1 : false;
?>
<div class="shirt_deals">
	<div class="container">
		<div class="controllerLine row">
			<div class="col-lg-4 left">
				<?php 
                    if ( function_exists('yoast_breadcrumb') ) {
                        echo bootstrapStyleBreadCrumbs(yoast_breadcrumb('','',false));
                    } 
                ?>
			</div>
			<div class="col-lg-4 center">
				<div class="wrapper">
					<?php
						the_archive_title( '<big>', '</big>' );
						the_archive_description( '<div class="taxonomy-description">', '</div>' );
					?>
				</div>
			</div>
			<div class="col-lg-4 right">
				<div class="wrapper">
					<?php 
						//echo "Showing ". $PageStarts ."&mdash;". $PageEnds . " of " . $postCount;
					?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="mainColumn col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row mainContent">
				<?php 

					for($j=0;$j<count($shirt_loop);$j++){
						//if(!get_field('deal_of_the_week', $shirt_loop[$j]->ID)){
					    	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $shirt_loop[$j]->ID ), 'single-post-thumbnail' );
					    	$title = $shirt_loop[$j]->post_title;
					    	$excerpt = $shirt_loop[$j]->post_excerpt;
					    	$content = $shirt_loop[$j]->post_content;
					    	$price = get_field('price', $shirt_loop[$j]->ID);
					    	$fits = get_the_category($shirt_loop[$j]->ID);
					    	$fitsLastName = end($fits)->name;
					    	$wbuy = get_field('where_to_buy_url', $shirt_loop[$j]->ID);
					    	$stars = get_field('stars', $shirt_loop[$j]->ID); //strlen()
					    	$picstars = "";
					    	$postlink = get_permalink($shirt_loop[$j]->ID);
					    	for ($i2=0;$i2<5;$i2++){
							    $picstars .= ($i2 < $stars) ? "<i class='glyphicon glyphicon-star'></i>" : "<i class='glyphicon glyphicon-star-empty'></i>";
							}
					    	?>
							<div class='col-md-4 col-lg-3 col-sm-12 col-xs-12 productBox productBox-<?php echo $j; ?>'>
							<div class="inner-wrap">
						        <a href="<?php echo $postlink; ?>">
						            <div class="product-image" style="background-image: url(<?php echo $image[0]; ?>)">
						                <img src='<?php echo esc_url( get_template_directory_uri() ); ?>/img/spacer.png' alt='background image'>
						                <div class="quick-view" data-prod="229">Learn More</div>
						            </div>
						            <!-- end product-image -->
						        </a>
						        <div class="info style-grid1">
						            <div class="text-center">
						                <h5 class="category">
						                	<?php
											foreach($fits as $fit){
								    			echo ($fit->name == $fitsLastName) ? $fit->name . "\n" : $fit->name . ", \n";
								    		}
								    		?>
						                </h5>
						                <div class="tx-div small center"></div>
						                <a href="<?php echo $postlink; ?>">
						                    <p class="name"><?php echo $title; ?></p>
						                </a>
						                <div class="star-rating" title="Rated <?php echo $stars; ?> out of 5"><span style="width:60%"><?php echo $picstars; ?></span></div>
						                <span class="price"><span class="amount">$<?php echo number_format($price,2,".",","); ?></span></span>
						            </div>
						        </div><!-- end info -->
						    </div><!-- .inner-wrap -->
							</div><!-- .box -->

				<?php
			        }
				?>
				</div> <!-- .row -->
			</div> <!-- .mainColumn -->