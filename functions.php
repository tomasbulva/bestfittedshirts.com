<?php
$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
function include_scripts_styles() {
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );

	// Load JavaScripts
	wp_enqueue_script( 'script-flickity', get_template_directory_uri() . '/js/flickity.pkgd.min.js', '', '2015-12-10', true );
	wp_enqueue_script( 'script-modernizr', get_template_directory_uri() . '/js/modernizr.js', array( 'jquery' ), '2015-10-21', true );
	wp_enqueue_script( 'script-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '2015-10-21', true );
	wp_enqueue_script( 'script-waypoints', get_template_directory_uri() . '/js/jquery.waypoints.min.js', array( 'jquery' ), '2015-11-24', true );
	wp_enqueue_script( 'script-waypoints-sticky', get_template_directory_uri() . '/js/sticky.min.js', array( 'jquery','script-waypoints' ), '2015-11-24', true );
	wp_enqueue_script( 'script-mailchimp-validator', $protocol.'s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js', array( '' ), '2015-11-23', true );
	//wp_enqueue_script( 'script-essentials', get_template_directory_uri() . '/js/essentials.js', array( 'jquery' ), '2015-10-21', true );
	wp_enqueue_script( 'script-functions', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '2015-10-21', true );

	//var_dump(get_post_type());

	// Loads our stylesheets.
	//wp_enqueue_style( 'style-social-icons', get_template_directory_uri() . '/fonts/ss-social-regular/webfonts/ss-social-regular.css', array(), '2015-10-21' );
	wp_enqueue_style( 'style-flickity', get_template_directory_uri() . '/css/flickity.min.css', array(), '2015-10-21' );
	wp_enqueue_style( 'style-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', false, '4' );
	wp_enqueue_style( 'style-bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array('style-bootstrap'), '4' );
	wp_enqueue_style( 'style-font-theme', '//fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic,300italic,300', false, '2015-10-21' );
	wp_enqueue_style( 'style-font-theme2', '//fonts.googleapis.com/css?family=Dancing+Script:400,700', false, '2015-10-21' );
	wp_enqueue_style( 'style-font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', false, '4.5.0' );
	
	//wp_enqueue_style( 'style-theme', get_template_directory_uri() . '/css/theme.css', array('style-bootstrap'), '2015-10-21' );
	//wp_enqueue_style( 'style-additional', get_template_directory_uri() . '/css/additional.css', array(), '2015-10-21' );
}
add_action( 'wp_enqueue_scripts', 'include_scripts_styles' );

// IE fixes
//<!--[if lt IE 10]> <![endif]-->
add_action( 'wp_head', create_function( '',
    'echo \'<!--[if lt IE 9]><script src="'.get_template_directory_uri().'/js/html5.js"></script>\';
     echo \'<script src="<?php echo get_template_directory_uri(); ?>js/respond.min.js"></script><![endif]-->\';'
) );


function theme_setup() {
	register_nav_menus( array(
	    "primary-nav" => "Main Menu",
	    "secondary-nav" => "Secondary Menu",
	) );
	
	add_theme_support( 'post-thumbnails' );
	add_post_type_support( 'page', array('excerpt','revisions') );
	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
			'page_title' 	=> 'Theme Options',
			'menu_title'	=> 'Theme Options',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}
}
add_action( 'after_setup_theme', 'theme_setup' );


//====== READ MORE link ======//
function modify_read_more_link() {
	return '<a class="more-link btn btn-tertiary" href="' . get_permalink() . '">Learn More</a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );


function allCustomPostTypes(){
	return array(
		'carousel',
		'industries_items',
		'services_items',
		'opinions_items',
		'careers_items',
		'post',
		//'page',
	);
}


function shirts() {
	$labels = array(
		'name'               => _x( 'Shirt', 'post type general name' ),
		'singular_name'      => _x( 'Shirt', 'post type singular name' ),
		'add_new'            => _x( 'Add New Shirt', 'book' ),
		'add_new_item'       => __( 'Add New Shirt' ),
		'edit_item'          => __( 'Edit Shirt' ),
		'new_item'           => __( 'New Shirt' ),
		'all_items'          => __( 'View All Shirts' ),
		'view_item'          => __( 'View Shirt' ),
		'search_items'       => __( 'Search Shirts' ),
		'not_found'          => __( 'No Shirts found' ),
		'not_found_in_trash' => __( 'No Shirts found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'Shirts'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'All Shirts',
		'public'        => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'shirt', 'with_front' => true, 'pages' => true, 'feeds' => true),
		'capability_type' => 'post',
		'menu_position' => 4,
		'show_in_menu'  => true,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments','revisions' ),
        'taxonomies' 	=> array('category','post_tag'),
		'has_archive'   => true,
	);
	register_post_type( 'shirt', $args );	
}
add_action( 'init', 'shirts' );


// function blog_posts() {
// 	$labels = array(
// 		'name'               => _x( 'Blog', 'post type general name' ),
// 		'singular_name'      => _x( 'Blog Posts', 'post type singular name' ),
// 		'add_new'            => _x( 'Add New Blog Post', 'book' ),
// 		'add_new_item'       => __( 'Add New Blog Post' ),
// 		'edit_item'          => __( 'Edit Blog Post' ),
// 		'new_item'           => __( 'New Blog Post' ),
// 		'all_items'          => __( 'View All Blog Posts' ),
// 		'view_item'          => __( 'View Blog Post' ),
// 		'search_items'       => __( 'Search Blog Post' ),
// 		'not_found'          => __( 'No Blog Posts found' ),
// 		'not_found_in_trash' => __( 'No Blog Posts in the Trash' ), 
// 		'parent_item_colon'  => '',
// 		'menu_name'          => 'Blog Posts'
// 	);
// 	$args = array(
// 		'labels'        => $labels,
// 		'description'   => 'Blog Posts',
// 		'public'        => true,
// 		'publicly_queryable' => true,
// 		'show_ui' => true,
// 		'query_var' => true,
// 		'rewrite' => array('slug' => 'blog_post', 'with_front' => true, 'pages' => true, 'feeds' => true),
// 		'capability_type' => 'post',
// 		'menu_position' => 4,
// 		'show_in_menu'  => true,
// 		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments','revisions' ),
//         'taxonomies' 	=> array('category','post_tag'),
// 		'has_archive'   => true,
// 	);
// 	register_post_type( 'blog_post', $args );	
// }
// add_action( 'init', 'blog_posts' );

function edit_admin_menus() {
	global $menu;
    global $submenu;
	
	//var_dump($submenu);

    $menu[5][0] = 'Blog'; // Change Posts to Recipes
    $submenu['edit.php'][5][0] = 'All Blog Posts';
    $submenu['edit.php'][10][0] = 'Add New Blog Post';

    $menu[20][0] = 'Misc Pages'; // Change Posts to Recipes
    $submenu['edit.php?post_type=page'][5][0] = 'All Misc Pages';
    $submenu['edit.php?post_type=page'][10][0] = 'Add New Misc Page';
}

add_action( 'admin_menu', 'edit_admin_menus', 999 );


function custom_menu_order($menu_ord) {
    if (!$menu_ord) return true;
    
    //var_dump($menu_ord);

    return array(
        'index.php', // Dashboard
        'separator1', // First separator
  		'edit.php?post_type=shirt',
  		'post.php?post=5&action=edit'
  		
    );
}

add_filter( 'custom_menu_order', '__return_true' );
add_filter( 'menu_order', 'custom_menu_order' );




// ************************
// Dropbox menu conditioner
// ************************

/**
 * Class Name: wp_bootstrap_navwalker
 * GitHub URI: https://github.com/twittem/wp-bootstrap-navwalker
 * Description: A custom WordPress nav walker class to implement the Bootstrap 3 navigation style in a custom theme using the WordPress built in menu manager.
 * Version: 2.0.4
 * Author: Edward McIntyre - @twittem
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */
class wp_bootstrap_navwalker extends Walker_Nav_Menu {
	/**
	 * @see Walker::start_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of page. Used for padding.
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "\n$indent<ul role=\"menu\" class=\" dropdown-menu\">\n";
	}
	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param int $current_page Menu item ID.
	 * @param object $args
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		/**
		 * Dividers, Headers or Disabled
		 * =============================
		 * Determine whether the item is a Divider, Header, Disabled or regular
		 * menu item. To prevent errors we use the strcasecmp() function to so a
		 * comparison that is not case sensitive. The strcasecmp() function returns
		 * a 0 if the strings are equal.
		 */
		if ( strcasecmp( $item->attr_title, 'divider' ) == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="divider">';
		} else if ( strcasecmp( $item->title, 'divider') == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="divider">';
		} else if ( strcasecmp( $item->attr_title, 'dropdown-header') == 0 && $depth === 1 ) {
			$output .= $indent . '<li role="presentation" class="dropdown-header">' . esc_attr( $item->title );
		} else if ( strcasecmp($item->attr_title, 'disabled' ) == 0 ) {
			$output .= $indent . '<li role="presentation" class="disabled"><a href="#">' . esc_attr( $item->title ) . '</a>';
		} else {
			$class_names = $value = '';
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = 'menu-item-' . $item->ID;
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
			if ( $args->has_children )
				$class_names .= ' dropdown';
			if ( in_array( 'current-menu-item', $classes ) )
				$class_names .= ' active';
			$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
			$output .= $indent . '<li' . $id . $value . $class_names .'>';
			$atts = array();
			$atts['title']  = ! empty( $item->title )	? $item->title	: '';
			$atts['target'] = ! empty( $item->target )	? $item->target	: '';
			$atts['rel']    = ! empty( $item->xfn )		? $item->xfn	: '';

			//print_r($item->href,false);//$atts['href']; //

			// If item has_children add atts to a.
			if ( $args->has_children && $depth === 0 ) {
				$atts['href']   		=  $item->url; //'#'.print_r(,false);
				$atts['data-toggle']	= 'dropdown';
				$atts['class']			= 'dropdown-toggle disabled';
				$atts['aria-haspopup']	= 'true';
			} else {
				$atts['href'] = ! empty( $item->url ) ? $item->url : '';
			}
			$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );
			$attributes = '';
			foreach ( $atts as $attr => $value ) {
				if ( ! empty( $value ) ) {
					$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
					$attributes .= ' ' . $attr . '="' . $value . '"';
				}
			}
			$item_output = $args->before;
			/*
			 * Glyphicons
			 * ===========
			 * Since the the menu item is NOT a Divider or Header we check the see
			 * if there is a value in the attr_title property. If the attr_title
			 * property is NOT null we apply it as the class name for the glyphicon.
			 */
			if ( ! empty( $item->attr_title ) )
				$item_output .= '<a'. $attributes .'><span class="glyphicon ' . esc_attr( $item->attr_title ) . '"></span>&nbsp;';
			else
				$item_output .= '<a'. $attributes .'>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= ( $args->has_children && 0 === $depth ) ? ' <span class="__caret"></span></a>' : '</a>';
			$item_output .= $args->after;
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}
	/**
	 * Traverse elements to create list from elements.
	 *
	 * Display one element if the element doesn't have any children otherwise,
	 * display the element and its children. Will only traverse up to the max
	 * depth and no ignore elements under that depth.
	 *
	 * This method shouldn't be called directly, use the walk() method instead.
	 *
	 * @see Walker::start_el()
	 * @since 2.5.0
	 *
	 * @param object $element Data object
	 * @param array $children_elements List of elements to continue traversing.
	 * @param int $max_depth Max depth to traverse.
	 * @param int $depth Depth of current element.
	 * @param array $args
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return null Null on failure with no changes to parameters.
	 */
	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
        if ( ! $element )
            return;
        $id_field = $this->db_fields['id'];
        // Display this element.
        if ( is_object( $args[0] ) )
           $args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );
        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
	/**
	 * Menu Fallback
	 * =============
	 * If this function is assigned to the wp_nav_menu's fallback_cb variable
	 * and a manu has not been assigned to the theme location in the WordPress
	 * menu manager the function with display nothing to a non-logged in user,
	 * and will add a link to the WordPress menu manager if logged in as an admin.
	 *
	 * @param array $args passed from the wp_nav_menu function.
	 *
	 */
	public static function fallback( $args ) {
		if ( current_user_can( 'manage_options' ) ) {
			extract( $args );
			$fb_output = null;
			if ( $container ) {
				$fb_output = '<' . $container;
				if ( $container_id )
					$fb_output .= ' id="' . $container_id . '"';
				if ( $container_class )
					$fb_output .= ' class="' . $container_class . '"';
				$fb_output .= '>';
			}
			$fb_output .= '<ul';
			if ( $menu_id )
				$fb_output .= ' id="' . $menu_id . '"';
			if ( $menu_class )
				$fb_output .= ' class="' . $menu_class . '"';
			$fb_output .= '>';
			$fb_output .= '<li><a href="' . admin_url( 'nav-menus.php' ) . '">Add a menu</a></li>';
			$fb_output .= '</ul>';
			if ( $container )
				$fb_output .= '</' . $container . '>';
			echo $fb_output;
		}
	}
}

function my_post_thumbnail() {
    if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
        return;
    }
 
    if ( is_singular() ) :
    ?>
 
    <div class="post-thumbnail">
        <?php the_post_thumbnail('full', array( 'alt' => get_the_title(), 'class' => 'img-responsive', )); ?>
    </div><!-- .post-thumbnail -->
 
    <?php else : ?>
 
    <a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
        <?php
            the_post_thumbnail( 'post-thumbnail', array( 'alt' => get_the_title(), 'class' => 'img-responsive', ) );
        ?>
    </a>
 
    <?php endif; // End is_singular()
}

function my_entry_meta() {
	if ( is_sticky() && is_home() && ! is_paged() ) {
		printf( '<span class="sticky-post">%s</span>', __( 'Featured', 'twentyfifteen' ) );
	}

	$format = get_post_format();
	if ( current_theme_supports( 'post-formats', $format ) ) {
		printf( '<span class="entry-format">%1$s<a href="%2$s">%3$s</a></span>',
			sprintf( '<span class="screen-reader-text">%s </span>', _x( 'Format', 'Used before post format.', 'twentyfifteen' ) ),
			esc_url( get_post_format_link( $format ) ),
			get_post_format_string( $format )
		);
	}

	if ( in_array( get_post_type(), array( 'post', 'attachment' ) ) ) {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			get_the_date(),
			esc_attr( get_the_modified_date( 'c' ) ),
			get_the_modified_date()
		);

		printf( '<span class="posted-on"><span class="screen-reader-text">%1$s </span><a href="%2$s" rel="bookmark">%3$s</a></span>',
			_x( 'Posted on', 'Used before publish date.', 'twentyfifteen' ),
			esc_url( get_permalink() ),
			$time_string
		);
	}

	if ( 'post' == get_post_type() ) {
		if ( is_singular() || is_multi_author() ) {
			printf( '<span class="byline"><span class="author vcard"><span class="screen-reader-text">%1$s </span><a class="url fn n" href="%2$s">%3$s</a></span></span>',
				_x( 'Author', 'Used before post author name.', 'twentyfifteen' ),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				get_the_author()
			);
		}

		$categories_list = get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfifteen' ) );
		if ( $categories_list && twentyfifteen_categorized_blog() ) {
			printf( '<span class="cat-links"><span class="screen-reader-text">%1$s </span>%2$s</span>',
				_x( 'Categories', 'Used before category names.', 'twentyfifteen' ),
				$categories_list
			);
		}

		$tags_list = get_the_tag_list( '', _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfifteen' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links"><span class="screen-reader-text">%1$s </span>%2$s</span>',
				_x( 'Tags', 'Used before tag names.', 'twentyfifteen' ),
				$tags_list
			);
		}
	}

	if ( is_attachment() && wp_attachment_is_image() ) {
		// Retrieve attachment metadata.
		$metadata = wp_get_attachment_metadata();

		printf( '<span class="full-size-link"><span class="screen-reader-text">%1$s </span><a href="%2$s">%3$s &times; %4$s</a></span>',
			_x( 'Full size', 'Used before full size attachment link.', 'twentyfifteen' ),
			esc_url( wp_get_attachment_url() ),
			$metadata['width'],
			$metadata['height']
		);
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( __( 'Leave a comment', 'twentyfifteen' ), __( '1 Comment', 'twentyfifteen' ), __( '% Comments', 'twentyfifteen' ) );
		echo '</span>';
	}
}

function twentyfifteen_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'twentyfifteen_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'twentyfifteen_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so twentyfifteen_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so twentyfifteen_categorized_blog should return false.
		return false;
	}
}

if ( ! function_exists( 'flatsome_posted_on' ) ) :


    
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function flatsome_posted_on() {
    $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
    if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
        $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
    }

    $time_string = sprintf( $time_string,
        esc_attr( get_the_date( 'c' ) ),
        esc_html( get_the_date() ),
        esc_attr( get_the_modified_date( 'c' ) ),
        esc_html( get_the_modified_date() )
    );

    $posted_on = sprintf(
        esc_html_x( 'Posted on %s', 'post date', 'flatsome' ),
        '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
    );

    $byline = sprintf(
        esc_html_x( 'by %s', 'post author', 'flatsome' ),
        '<span class="meta-author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
    );

    echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>';

}
endif;


/**
 * Flush out the transients used in {@see twentyfifteen_categorized_blog()}.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'twentyfifteen_categories' );
}
add_action( 'edit_category', 'twentyfifteen_category_transient_flusher' );
add_action( 'save_post',     'twentyfifteen_category_transient_flusher' );

function process_tag($tags){

	$regex3 = "#(.*class=')(.*)(' title.*)#e";
    $tagn = [];
    foreach( $tags as $tag ) {
        $tag = preg_replace($regex3, "('$1btn btn-transparent $2$3')", $tag );
        $tagn[] = $tag;
    }
	return $tagn;
}

add_filter ( 'wp_tag_cloud', 'tag_cloud_class' );
function tag_cloud_class( $tags ) {
	if (!is_array($tags)) {
        $tags = explode("</a>", $tags);
        //$tags = process_tag($tags);

        $args = array(
			'smallest'                  => 14, 
			'largest'                   => 14,
			'unit'                      => 'px', 
			'number'                    => 45,  
			'format'                    => 'array',
			'separator'                 => "\n",
			'orderby'                   => 'name', 
			'order'                     => 'ASC',
			'exclude'                   => null, 
			'include'                   => null, 
			'topic_count_text_callback' => default_topic_count_text,
			'link'                      => 'view', 
			'taxonomy'                  => 'post_tag', 
			'echo'                      => true,
			'child_of'                  => null, // see Note!
		);
        $output = "";

        $defaults = array(
			'smallest' => 8, 'largest' => 22, 'unit' => 'pt', 'number' => 45,
			'format' => 'flat', 'separator' => "\n", 'orderby' => 'name', 'order' => 'ASC',
			'exclude' => '', 'include' => '', 'link' => 'view', 'taxonomy' => 'post_tag', 'post_type' => '', 'echo' => true
		);

        $args = wp_parse_args( $args, $defaults );

		$tags = get_terms( $args['taxonomy'], array_merge( $args, array( 'orderby' => 'count', 'order' => 'DESC' ) ) ); // Always query top tags

		if ( empty( $tags ) || is_wp_error( $tags ) )
			return;

		foreach ( $tags as $key => $tag ) {
			if ( 'edit' == $args['link'] )
				$link = get_edit_term_link( $tag->term_id, $tag->taxonomy, $args['post_type'] );
			else
				$link = get_term_link( intval($tag->term_id), $tag->taxonomy );
			if ( is_wp_error( $link ) )
				return;

			$tags[ $key ]->link = $link;
			$tags[ $key ]->id = $tag->term_id;
		}

		$mytags = wp_generate_tag_cloud( $tags, $args ); // Here's where those top tags get sorted according to $args

		//print_r($mytags);

		$return = process_tag($mytags);

		// if ( 'array' == $args['format'] || empty($args['echo']) )
		// 	return $return;
        
		//print_r($return);

        $cloudtags = $return;
		foreach( $cloudtags as $tag ){
			 $output .= $tag . " ";
		}
    	return $output;

    }else{
    	return process_tag($tags);
    }
    
	
}

function bootstrapStyleBreadCrumbs($rawBread){
	$dom = new DOMDocument();
	$dom->loadHTML($rawBread);
	// print_r("\n\n\n");
	// print_r($rawBread);
	// print_r("\n\n\n");
	$as = $dom->getElementsByTagName('a');

	$finder = new DomXPath($dom);
	$classname="breadcrumb_last";
	$strong = $finder->query("//*[contains(@class, '$classname')]");
	$shirtsURL = site_url( '/latest-shirt-deals/');
	//$strong = $dom->getElementsByTagName('span');

	//print_r($as);
	//print_r($strong);
	//childNodes
	$output = "<ol class='breadcrumb'>";
	foreach ($as as $a) {
		//print_r($a->nodeValue); //->attributes->item(0)->name
		//print_r($a->attributes->item(0)->value);
		//if($a->attributes->item(0)->value == "v:Breadcrumb"){
			if($a->attributes->item(0)->name == "href"){
				//this is a dirty hack to change custom post type link into the best deals link
				switch(strtolower($a->nodeValue)){
					case "shirt":
						$output .= "<li>\n<a href='". $shirtsURL ."'>Latest Shirt Deals</a>\n</li>\n\n";
						break;
					default:
						$output .= "<li>\n<a href='". $a->attributes->item(0)->value ."'>".$a->childNodes->item(0)->nodeValue . "</a>\n</li>\n\n";
						break;
				}
			}
		//}
	}
	//print_r($strong->item(0));
	$output .= "<li property='v:title' class='active'>\n";
	$output .= $strong->item(0)->nodeValue;
	$output .= "</li>\n\n";
	$output .= "</ol>\n\n\n";

	return $output;
}


/*
Name:  WordPress Post Like System
Description:  A simple and efficient post like system for WordPress.
Version:      0.5.2
Author:       Jon Masterson
Author URI:   http://jonmasterson.com/
License:
Copyright (C) 2015 Jon Masterson
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Register the stylesheets for the public-facing side of the site.
 * @since    0.5
 */
add_action( 'wp_enqueue_scripts', 'sl_enqueue_scripts' );
function sl_enqueue_scripts() {
	// wp_enqueue_script( 'simple-likes-public-js', get_template_directory_uri() . '/js/simple-likes-public.js', array( 'jquery' ), '0.5', false );
	// wp_localize_script( 'simple-likes-public-js', 'simpleLikes', array(
	// 	'ajaxurl' => admin_url( 'admin-ajax.php' ),
	// 	'like' => __( 'Like', 'YourThemeTextDomain' ),
	// 	'unlike' => __( 'Unlike', 'YourThemeTextDomain' )
	// ) ); 
}

/**
 * Processes like/unlike
 * @since    0.5
 */
add_action( 'wp_ajax_nopriv_process_simple_like', 'process_simple_like' );
add_action( 'wp_ajax_process_simple_like', 'process_simple_like' );
function process_simple_like() {
	// Security
	$nonce = isset( $_REQUEST['nonce'] ) ? sanitize_text_field( $_REQUEST['nonce'] ) : 0;
	if ( !wp_verify_nonce( $nonce, 'simple-likes-nonce' ) ) {
		exit( __( 'Not permitted', 'YourThemeTextDomain' ) );
	}
	// Test if javascript is disabled
	$disabled = ( isset( $_REQUEST['disabled'] ) && $_REQUEST['disabled'] == true ) ? true : false;
	// Test if this is a comment
	$is_comment = ( isset( $_REQUEST['is_comment'] ) && $_REQUEST['is_comment'] == 1 ) ? 1 : 0;
	// Base variables
	$post_id = ( isset( $_REQUEST['post_id'] ) && is_numeric( $_REQUEST['post_id'] ) ) ? $_REQUEST['post_id'] : '';
	$result = array();
	$post_users = NULL;
	$like_count = 0;
	// Get plugin options
	if ( $post_id != '' ) {
		$count = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_comment_like_count", true ) : get_post_meta( $post_id, "_post_like_count", true ); // like count
		$count = ( isset( $count ) && is_numeric( $count ) ) ? $count : 0;
		if ( !already_liked( $post_id, $is_comment ) ) { // Like the post
			if ( is_user_logged_in() ) { // user is logged in
				$user_id = get_current_user_id();
				$post_users = post_user_likes( $user_id, $post_id, $is_comment );
				if ( $is_comment == 1 ) {
					// Update User & Comment
					$user_like_count = get_user_option( "_comment_like_count", $user_id );
					$user_like_count =  ( isset( $user_like_count ) && is_numeric( $user_like_count ) ) ? $user_like_count : 0;
					update_user_option( $user_id, "_comment_like_count", ++$user_like_count );
					if ( $post_users ) {
						update_comment_meta( $post_id, "_user_comment_liked", $post_users );
					}
				} else {
					// Update User & Post
					$user_like_count = get_user_option( "_user_like_count", $user_id );
					$user_like_count =  ( isset( $user_like_count ) && is_numeric( $user_like_count ) ) ? $user_like_count : 0;
					update_user_option( $user_id, "_user_like_count", ++$user_like_count );
					if ( $post_users ) {
						update_post_meta( $post_id, "_user_liked", $post_users );
					}
				}
			} else { // user is anonymous
				$user_ip = sl_get_ip();
				$post_users = post_ip_likes( $user_ip, $post_id, $is_comment );
				// Update Post
				if ( $post_users ) {
					if ( $is_comment == 1 ) {
						update_comment_meta( $post_id, "_user_comment_IP", $post_users );
					} else { 
						update_post_meta( $post_id, "_user_IP", $post_users );
					}
				}
			}
			$like_count = ++$count;
			$response['status'] = "liked";
			$response['icon'] = get_liked_icon();
		} else { // Unlike the post
			if ( is_user_logged_in() ) { // user is logged in
				$user_id = get_current_user_id();
				$post_users = post_user_likes( $user_id, $post_id, $is_comment );
				// Update User
				if ( $is_comment == 1 ) {
					$user_like_count = get_user_option( "_comment_like_count", $user_id );
					$user_like_count =  ( isset( $user_like_count ) && is_numeric( $user_like_count ) ) ? $user_like_count : 0;
					if ( $user_like_count > 0 ) {
						update_user_option( $user_id, "_comment_like_count", --$user_like_count );
					}
				} else {
					$user_like_count = get_user_option( "_user_like_count", $user_id );
					$user_like_count =  ( isset( $user_like_count ) && is_numeric( $user_like_count ) ) ? $user_like_count : 0;
					if ( $user_like_count > 0 ) {
						update_user_option( $user_id, '_user_like_count', --$user_like_count );
					}
				}
				// Update Post
				if ( $post_users ) {	
					$uid_key = array_search( $user_id, $post_users );
					unset( $post_users[$uid_key] );
					if ( $is_comment == 1 ) {
						update_comment_meta( $post_id, "_user_comment_liked", $post_users );
					} else { 
						update_post_meta( $post_id, "_user_liked", $post_users );
					}
				}
			} else { // user is anonymous
				$user_ip = sl_get_ip();
				$post_users = post_ip_likes( $user_ip, $post_id, $is_comment );
				// Update Post
				if ( $post_users ) {
					$uip_key = array_search( $user_ip, $post_users );
					unset( $post_users[$uip_key] );
					if ( $is_comment == 1 ) {
						update_comment_meta( $post_id, "_user_comment_IP", $post_users );
					} else { 
						update_post_meta( $post_id, "_user_IP", $post_users );
					}
				}
			}
			$like_count = ( $count > 0 ) ? --$count : 0; // Prevent negative number
			$response['status'] = "unliked";
			$response['icon'] = get_unliked_icon();
		}
		if ( $is_comment == 1 ) {
			update_comment_meta( $post_id, "_comment_like_count", $like_count );
			update_comment_meta( $post_id, "_comment_like_modified", date( 'Y-m-d H:i:s' ) );
		} else { 
			update_post_meta( $post_id, "_post_like_count", $like_count );
			update_post_meta( $post_id, "_post_like_modified", date( 'Y-m-d H:i:s' ) );
		}
		$response['count'] = get_like_count( $like_count );
		$response['testing'] = $is_comment;
		if ( $disabled == true ) {
			if ( $is_comment == 1 ) {
				wp_redirect( get_permalink( get_the_ID() ) );
				exit();
			} else {
				wp_redirect( get_permalink( $post_id ) );
				exit();
			}
		} else {
			wp_send_json( $response );
		}
	}
}

/**
 * Utility to test if the post is already liked
 * @since    0.5
 */
function already_liked( $post_id, $is_comment ) {
	$post_users = NULL;
	$user_id = NULL;
	if ( is_user_logged_in() ) { // user is logged in
		$user_id = get_current_user_id();
		$post_meta_users = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_user_comment_liked" ) : get_post_meta( $post_id, "_user_liked" );
		if ( count( $post_meta_users ) != 0 ) {
			$post_users = $post_meta_users[0];
		}
	} else { // user is anonymous
		$user_id = sl_get_ip();
		$post_meta_users = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_user_comment_IP" ) : get_post_meta( $post_id, "_user_IP" ); 
		if ( count( $post_meta_users ) != 0 ) { // meta exists, set up values
			$post_users = $post_meta_users[0];
		}
	}
	if ( is_array( $post_users ) && in_array( $user_id, $post_users ) ) {
		return true;
	} else {
		return false;
	}
} // already_liked()

function get_simple_comments_button( $post_id ){
	$num_comments = get_comments_number($post_id); // get_comments_number returns only a numeric value
	$icon = get_comment_icon();
	$icon_off = get_off_comment_icon();

	if ( comments_open($post_id) ) {
		$output = '<span class="cm-wrapper"><a href="' . get_comments_link($post_id) .'">' . $icon . '<span class="cm-count">' . $num_comments . '</span></a></span>';
	} else {
		$output = '<span class="cm-wrapper">' . $icon_off . '</span>';
	}

	

	return $output;
}


/**
 * Output the like button
 * @since    0.5
 */
function get_simple_likes_button( $post_id, $is_comment = NULL ) {
	$is_comment = ( NULL == $is_comment ) ? 0 : 1;
	$large = ( NULL == $is_comment ) ? 0 : 1;
	$output = '';
	$nonce = wp_create_nonce( 'simple-likes-nonce' ); // Security
	if ( $is_comment == 1 ) {
		$post_id_class = esc_attr( ' sl-comment-button-' . $post_id );
		$comment_class = esc_attr( ' sl-comment' );
		$like_count = get_comment_meta( $post_id, "_comment_like_count", true );
		$like_count = ( isset( $like_count ) && is_numeric( $like_count ) ) ? $like_count : 0;
	} else {
		$post_id_class = esc_attr( ' sl-button-' . $post_id );
		$comment_class = esc_attr( '' );
		$like_count = get_post_meta( $post_id, "_post_like_count", true );
		$like_count = ( isset( $like_count ) && is_numeric( $like_count ) ) ? $like_count : 0;
	}
	$count = get_like_count( $like_count );
	$icon_empty = get_unliked_icon();
	$icon_full = get_liked_icon();
	// Loader
	$loader = '<span id="sl-loader"></span>';
	// Liked/Unliked Variables
	if ( already_liked( $post_id, $is_comment ) ) {
		$class = esc_attr( ' liked' );
		$title = __( 'Unlike', 'YourThemeTextDomain' );
		$icon = $icon_full;
	} else {
		$class = '';
		$title = __( 'Like', 'YourThemeTextDomain' );
		$icon = $icon_empty;
	}
	$output = '<span class="sl-wrapper"><a href="' . admin_url( 'admin-ajax.php?action=process_simple_like' . '&nonce=' . $nonce . '&post_id=' . $post_id . '&disabled=true&is_comment=' . $is_comment ) . '" class="sl-button' . $post_id_class . $class . $comment_class . '" data-nonce="' . $nonce . '" data-post-id="' . $post_id . '" data-iscomment="' . $is_comment . '" title="' . $title . '">' . $icon . $count . '</a>' . $loader . '</span>';
	return $output;
} // get_simple_likes_button()

/**
 * Processes shortcode to manually add the button to posts
 * @since    0.5
 */
add_shortcode( 'jmliker', 'sl_shortcode' );
function sl_shortcode() {
	return get_simple_likes_button( get_the_ID(), 0 );
} // shortcode()

/**
 * Utility retrieves post meta user likes (user id array), 
 * then adds new user id to retrieved array
 * @since    0.5
 */
function post_user_likes( $user_id, $post_id, $is_comment ) {
	$post_users = '';
	$post_meta_users = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_user_comment_liked" ) : get_post_meta( $post_id, "_user_liked" );
	if ( count( $post_meta_users ) != 0 ) {
		$post_users = $post_meta_users[0];
	}
	if ( !is_array( $post_users ) ) {
		$post_users = array();
	}
	if ( !in_array( $user_id, $post_users ) ) {
		$post_users['user-' . $user_id] = $user_id;
	}
	return $post_users;
} // post_user_likes()

/**
 * Utility retrieves post meta ip likes (ip array), 
 * then adds new ip to retrieved array
 * @since    0.5
 */
function post_ip_likes( $user_ip, $post_id, $is_comment ) {
	$post_users = '';
	$post_meta_users = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_user_comment_IP" ) : get_post_meta( $post_id, "_user_IP" );
	// Retrieve post information
	if ( count( $post_meta_users ) != 0 ) {
		$post_users = $post_meta_users[0];
	}
	if ( !is_array( $post_users ) ) {
		$post_users = array();
	}
	if ( !in_array( $user_ip, $post_users ) ) {
		$post_users['ip-' . $user_ip] = $user_ip;
	}
	return $post_users;
} // post_ip_likes()

/**
 * Utility to retrieve IP address
 * @since    0.5
 */
function sl_get_ip() {
	if ( isset( $_SERVER['HTTP_CLIENT_IP'] ) && ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) && ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = ( isset( $_SERVER['REMOTE_ADDR'] ) ) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
	}
	$ip = filter_var( $ip, FILTER_VALIDATE_IP );
	$ip = ( $ip === false ) ? '0.0.0.0' : $ip;
	return $ip;
} // sl_get_ip()

/**
 * Utility returns the button icon for "like" action
 * @since    0.5
 */
function get_liked_icon() {
	/* If already using Font Awesome with your theme, replace svg with: <i class="fa fa-heart"></i> */
	//$icon = '<span class="sl-icon"><svg role="img" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" viewBox="0 0 128 128" enable-background="new 0 0 128 128" xml:space="preserve"><path id="heart-full" d="M124 20.4C111.5-7 73.7-4.8 64 19 54.3-4.9 16.5-7 4 20.4c-14.7 32.3 19.4 63 60 107.1C104.6 83.4 138.7 52.7 124 20.4z"/>&#9829;</svg></span>';
	$icon = '<span class="sl-icon"><svg role="img" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" viewBox="0 0 128 128" enable-background="new 0 0 128 128" xml:space="preserve"><path id="thumbsup" d="M 117.39,66.74 C 111.23,67.28 103.02,66.85 96.44,64.45 88.98,61.74 87.05,60.77 82.88,54.35 79.76,49.54 77.27,45.18 74.5,39.83 71.94,34.87 67.41,31.64 64.77,27.04 59.3,17.52 63.81,1.88 52.42,-0 40.19,0.05 40.44,15.35 41.61,24.74 42.62,32.87 48.79,38.07 50.95,45.53 53.46,54.19 39.75,51.67 33.65,51.16 26.02,50.51 15.95,47.7 9.85,52.88 1.75,59.77 5.73,69 14.89,72.71 -0.33,83.15 15.65,93.38 15.65,93.38 15.65,93.38 9.72,98.32 11.04,101.72 12.91,106.65 17.3,109.45 21.44,111.28 12.85,121.65 29.82,124.75 36.55,125.43 46.95,126.47 56.78,127.49 66.3,123.78 75.02,120.39 80.23,114.34 89.69,112.51 100.35,110.47 111.28,117.8 119.48,115.55 127.68,113.3 123.55,66.2 117.39,66.74 Z M 117.39,66.74" /></svg></span>';
	return $icon;
} // get_liked_icon()

/**
 * Utility returns the button icon for "unlike" action
 * @since    0.5
 */
function get_unliked_icon() {
	/* If already using Font Awesome with your theme, replace svg with: <i class="fa fa-heart-o"></i> */
	//$icon = '<span class="sl-icon"><svg role="img" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" viewBox="0 0 128 128" enable-background="new 0 0 128 128" xml:space="preserve"><path id="heart" d="M64 127.5C17.1 79.9 3.9 62.3 1 44.4c-3.5-22 12.2-43.9 36.7-43.9 10.5 0 20 4.2 26.4 11.2 6.3-7 15.9-11.2 26.4-11.2 24.3 0 40.2 21.8 36.7 43.9C124.2 62 111.9 78.9 64 127.5zM37.6 13.4c-9.9 0-18.2 5.2-22.3 13.8C5 49.5 28.4 72 64 109.2c35.7-37.3 59-59.8 48.6-82 -4.1-8.7-12.4-13.8-22.3-13.8 -15.9 0-22.7 13-26.4 19.2C60.6 26.8 54.4 13.4 37.6 13.4z"/>&#9829;</svg></span>';
	$icon = '<span class="sl-icon"><svg role="img" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" viewBox="0 0 128 128" enable-background="new 0 0 128 128" xml:space="preserve"><path id="thumbsup" d="M 51.43,126 C 46.96,126 42.63,125.61 38.29,125.09 L 38.16,125.09 C 30.67,124.3 21.21,122.08 18.58,115.68 17.93,114.12 17.53,112.03 18.32,109.41 14.77,107.06 12.28,104.19 10.96,100.53 9.65,97.27 10.96,94.14 12.8,91.65 10.57,89.3 8.34,85.78 8.21,81.6 8.21,79.38 8.73,76.51 11.1,73.37 8.21,70.89 6.37,67.76 5.84,64.36 5.05,59.53 7.02,54.83 11.36,51.17 17.4,46.08 25.81,47.12 32.38,48.04 33.82,48.17 35.27,48.43 36.45,48.56 37.11,48.56 37.9,48.69 38.68,48.82 40.52,48.95 44.86,49.47 46.96,49.08 46.3,46.99 44.99,44.9 43.68,42.68 41.57,39.16 39.08,35.24 38.55,30.02 37.24,19.18 38.55,11.34 42.63,6.77 45.25,3.9 48.8,2.33 53.14,2.2 53.4,2.2 53.66,2.2 53.92,2.2 63.91,3.77 65.22,12.91 66.14,19.57 66.67,22.96 67.19,26.49 68.51,28.97 69.43,30.54 70.74,31.97 72.05,33.54 73.89,35.5 75.86,37.85 77.44,40.72 79.94,45.56 82.17,49.34 84.8,53.39 87.82,57.96 88.48,58.35 94.65,60.57 100.04,62.53 107,62.79 111.6,62.4 L 111.6,62.4 C 118.69,61.75 120.53,72.33 121.32,76.77 121.32,77.16 127.63,112.42 115.14,115.81 110.28,117.12 105.16,115.81 100.3,114.64 95.96,113.59 91.63,112.42 87.69,113.2 83.48,113.99 80.33,115.94 76.78,118.16 73.89,119.86 71,121.69 67.32,123.13 62.07,125.35 56.81,126 51.43,126 Z M 27.65,112.16 C 28.44,113.07 32.38,114.9 39.08,115.55 L 39.21,115.55 C 48.41,116.47 56.29,117.25 63.91,114.38 66.8,113.33 69.16,111.77 71.79,110.2 75.86,107.72 80.07,105.11 85.98,103.93 92.02,102.75 97.67,104.19 102.8,105.5 106.21,106.41 109.5,107.19 111.86,106.93 112.65,105.11 113.7,99.49 113.17,89.3 112.65,80.69 111.34,74.81 110.28,72.33 103.58,72.59 96.62,71.8 91.37,69.85 83.88,67.1 81.12,65.67 76.65,58.88 73.76,54.57 71.53,50.52 68.77,45.43 67.85,43.6 66.27,41.9 64.83,40.2 63.12,38.37 61.41,36.41 60.1,34.06 57.73,30.02 57.08,25.31 56.55,21.27 55.5,14.34 54.84,12.65 52.74,12.12 51.03,12.26 50.38,12.78 49.85,13.43 48.93,14.48 46.7,18.26 48.14,29.1 48.54,32.24 50.11,34.85 51.95,37.98 53.66,40.72 55.37,43.6 56.42,47.12 57.6,51.17 56.29,53.91 54.97,55.48 51.17,60.05 43.68,59.27 37.63,58.62 36.85,58.48 36.19,58.48 35.66,58.35 34.22,58.22 32.64,57.96 31.06,57.83 26.07,57.18 20.29,56.4 17.53,58.75 15.83,60.18 15.04,61.62 15.3,63.19 15.69,65.28 17.8,67.36 20.82,68.54 22.53,69.19 23.58,70.76 23.84,72.46 23.97,74.16 23.18,75.98 21.74,76.9 20.29,77.94 17.8,79.9 17.93,81.73 17.93,84.08 21.21,86.82 22.39,87.61 23.71,88.39 24.5,89.83 24.63,91.39 24.76,92.96 24.1,94.4 22.92,95.44 22,96.23 20.95,97.53 20.42,98.18 21.47,100.27 23.58,101.97 26.99,103.41 28.44,104.06 29.36,105.24 29.75,106.67 30.15,108.11 29.75,109.68 28.83,110.85 27.91,111.5 27.65,112.03 27.65,112.16 Z M 27.65,112.16" /></svg></span>';
	return $icon;
} // get_unliked_icon()

function get_comment_icon() {
	$icon = '<span class="cm-icon"><svg role="img" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" viewBox="0 0 128 128" enable-background="new 0 0 128 128" xml:space="preserve"><path id="thumbsup" d="M 125,58.26 C 125,85.46 97.91,107.51 64.5,107.51 55.23,107.51 46.45,105.82 38.6,102.78 22.47,114.81 -5.77,123.03 7.4,114.69 17.19,108.49 22.72,101.32 25.73,96.07 12.45,87.04 4,73.45 4,58.26 4,36.37 21.53,17.82 45.79,11.4 51.68,9.84 57.97,9 64.5,9 97.91,9 125,31.05 125,58.26 Z M 125,58.26" /></svg></span>';
	return $icon;
}

function get_off_comment_icon() {
	$icon = '<span class="cm-icon"><svg role="img" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0" y="0" viewBox="0 0 128 128" enable-background="new 0 0 128 128" xml:space="preserve"><path id="thumbsup" d="M 79.43,30.67 C 79.43,30.67 77.87,32.23 75.34,34.76 72.32,37.78 67.92,42.18 63.16,46.94 54.44,38.21 46.9,30.67 46.9,30.68 46.76,30.81 37,40.57 37,40.57 37,40.57 44.54,48.11 53.26,56.84 44.54,65.56 37,73.1 37,73.1 L 46.9,83 C 46.9,83 54.44,75.46 63.16,66.74 71.89,75.46 79.43,83 79.43,83 L 89.33,73.1 C 89.33,73.1 81.79,65.56 73.06,56.84 81.79,48.11 89.33,40.57 89.33,40.57 L 79.43,30.67 Z M 125,58.26 C 125,85.46 97.91,107.51 64.5,107.51 55.23,107.51 46.45,105.82 38.6,102.78 22.47,114.81 -5.77,123.03 7.4,114.69 17.19,108.49 22.72,101.32 25.73,96.07 12.45,87.04 4,73.45 4,58.26 4,40.22 15.91,24.45 33.67,15.86 37.46,14.03 41.52,12.53 45.79,11.4 51.68,9.84 57.97,9 64.5,9 97.91,9 125,31.05 125,58.26 Z M 125,58.26" /></svg></span>';
	return $icon;
}

/**
 * Utility function to format the button count,
 * appending "K" if one thousand or greater,
 * "M" if one million or greater,
 * and "B" if one billion or greater (unlikely).
 * $precision = how many decimal points to display (1.25K)
 * @since    0.5
 */
function sl_format_count( $number ) {
	$precision = 2;
	if ( $number >= 1000 && $number < 1000000 ) {
		$formatted = number_format( $number/1000, $precision ).'K';
	} else if ( $number >= 1000000 && $number < 1000000000 ) {
		$formatted = number_format( $number/1000000, $precision ).'M';
	} else if ( $number >= 1000000000 ) {
		$formatted = number_format( $number/1000000000, $precision ).'B';
	} else {
		$formatted = $number; // Number is less than 1000
	}
	$formatted = str_replace( '.00', '', $formatted );
	return $formatted;
} // sl_format_count()

/**
 * Utility retrieves count plus count options, 
 * returns appropriate format based on options
 * @since    0.5
 */
function get_like_count( $like_count ) {
	$like_text = __( 'Like', 'YourThemeTextDomain' );
	if ( is_numeric( $like_count ) && $like_count > 0 ) { 
		$number = sl_format_count( $like_count );
	} else {
		$number = $like_text;
	}
	$count = '<span class="sl-count">' . $number . '</span>';
	return $count;
} // get_like_count()

// User Profile List
add_action( 'show_user_profile', 'show_user_likes' );
add_action( 'edit_user_profile', 'show_user_likes' );
function show_user_likes( $user ) { ?>        
	<table class="form-table">
		<tr>
			<th><label for="user_likes"><?php _e( 'You Like:', 'YourThemeTextDomain' ); ?></label></th>
			<td>
			<?php
			$types = get_post_types( array( 'public' => true ) );
			$args = array(
			  'numberposts' => -1,
			  'post_type' => $types,
			  'meta_query' => array (
				array (
				  'key' => '_user_liked',
				  'value' => $user->ID,
				  'compare' => 'LIKE'
				)
			  ) );		
			$sep = '';
			$like_query = new WP_Query( $args );
			if ( $like_query->have_posts() ) : ?>
			<p>
			<?php while ( $like_query->have_posts() ) : $like_query->the_post(); 
			echo $sep; ?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
			<?php
			$sep = ' &middot; ';
			endwhile; 
			?>
			</p>
			<?php else : ?>
			<p><?php _e( 'You do not like anything yet.', 'YourThemeTextDomain' ); ?></p>
			<?php 
			endif; 
			wp_reset_postdata(); 
			?>
			</td>
		</tr>
	</table>
<?php } // show_user_likes()

// add_filter('next_post_link', 'post_link_attributes');
// add_filter('previous_post_link', 'post_link_attributes');
 
// function post_link_attributes($output) {
//     $code = 'class="prod-dropdown"';
//     return str_replace('<a href=', '<a '.$code.' href=', $output);
// }


if ( ! function_exists( 'flatsome_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function flatsome_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'flatsome' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'flatsome' ), '<span class="edit-link">', '<span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment-inner">

            <div class="row collapse">
                <div class="large-2 columns">
                    <div class="comment-author">
                    <?php echo get_avatar( $comment, 80 ); ?>
                </div>
                </div><!-- .large-3 -->

                <div class="large-10 columns">
                    <?php printf( __( '%s <span class="says">says:</span>', 'flatsome' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
                    <?php if ( $comment->comment_approved == '0' ) : ?>
                    <em><?php _e( 'Your comment is awaiting moderation.', 'flatsome' ); ?></em>
                    <br />
                     <?php endif; ?>

                <div class="comment-content"><?php comment_text(); ?></div>


                 <div class="comment-meta commentmetadata">
                    <a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"><time datetime="<?php comment_time( 'c' ); ?>">
                    <?php printf( _x( '%1$s at %2$s', '1: date, 2: time', 'flatsome' ), get_comment_date(), get_comment_time() ); ?>
                    </time></a>
                    <?php edit_comment_link( __( 'Edit', 'flatsome' ), '<span class="edit-link">', '<span>' ); ?>
                    
                    <div class="reply right">
            <?php
                comment_reply_link( array_merge( $args,array(
                    'depth'     => $depth,
                    'max_depth' => $args['max_depth'],
                ) ) );
            ?>
            </div><!-- .reply -->


                </div><!-- .comment-meta .commentmetadata -->

                </div><!-- .large-10 columns -->

            </div><!-- .row -->

		</article>
    <!-- #comment -->

	<?php
			break;
	endswitch;
}
endif; // ends check for flatsome_comment()


/**
 * Register widgetized area and update sidebar with default widgets
 */
function flatsome_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'flatsome' ),
		'id'            => 'sidebar-main',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3><div class="tx-div small"></div>',
	) );
}

add_action( 'widgets_init', 'flatsome_widgets_init' );








add_action( 'widgets_init', 'recent_posts_widget' );

function recent_posts_widget() {

	register_widget( 'Flatsome_Recent_Post_Widget' );
}

/**
 * Recent_Posts widget class
 *
 * @since 2.8.0
 */
class Flatsome_Recent_Post_Widget extends WP_Widget {
	
	function Flatsome_Recent_Post_Widget() {
		$widget_ops = array( 'classname' => 'flatsome_recent_posts', 'description' => __('A widget that displays recent posts ', 'flatsome') );
		
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'flatsome_recent_posts' );
		
		parent::__construct( 'flatsome_recent_posts', __('Special Recent Posts', 'flatsome'), $widget_ops, $control_ops );
	}

	function widget($args, $instance) {

		$cache = wp_cache_get('widget_recent_posts', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = apply_filters('widget_title', empty($instance['title']) ? __('Recent Posts', 'flatsome') : $instance['title'], $instance, $this->id_base);
		if ( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) )
 			$number = 10;

		$r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true ) ) );
		if ($r->have_posts()) :
		
		echo $before_widget; 

		if ( $title ) echo $before_title . $title . $after_title; ?>
		<ul>
		<?php while ( $r->have_posts() ) : $r->the_post(); ?>
			<li>
				<div class="post-date">
	                    <span class="post-date-day"><?php echo get_the_time('d'); ?></span>
	                    <span class="post-date-month"><?php echo get_the_time('M'); ?></span>
                </div>
         
                <a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() ? get_the_title() : get_the_ID() ); ?>"><?php if ( get_the_title() ) the_title(); else the_ID(); ?></a>
				<div class="post_comments"><?php comments_popup_link( __( 'Leave a comment', 'flatsome' ), __( '<strong>1</strong> Comment', 'flatsome' ), __( '<strong>%</strong> Comments', 'flatsome' ) ); ?></div>
            </li>
		<?php endwhile; ?>
		</ul>
		<?php 
		echo $after_widget; 

		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;

		$cache[$args['widget_id']] = ob_get_flush();
		wp_cache_set('widget_recent_posts', $cache, 'widget');
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_entries']) )
			delete_option('widget_recent_entries');

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete('widget_recent_posts', 'widget');
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		
		?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'flatsome' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'flatsome' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
<?php
	}
}




if ( ! function_exists( 'twentyfifteen_post_thumbnail' ) ) :
/**
 * Display an optional post thumbnail.
 *
 * Wraps the post thumbnail in an anchor element on index views, or a div
 * element when on single views.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_post_thumbnail() {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		return;
	}

	if ( is_singular() ) :
	?>

	<div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div><!-- .post-thumbnail -->

	<?php else : ?>

	<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
		<?php
			the_post_thumbnail( 'post-thumbnail', array( 'alt' => get_the_title() ) );
		?>
	</a>

	<?php endif; // End is_singular()
}
endif;


function comment_args(){
	return array( 
		'class_submit'      => 'btn btn-primary',
		
		'comment_field' => '<div class="form-group"><label for="comment">' . __( 'Comment:' ) . '</label>' .
	                '<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" class="form-control"></textarea></div>' .
	                '<!-- #form-section-comment .form-section -->',

		'fields' => apply_filters( 'comment_form_default_fields', array(

	    'author' => '<div class="form-group"><label for="author">' . __( 'Your Name:' ) . '</label> ' .
	                ( $req ? '<span class="required">*</span>' : '' ) .
	                '<input id="author" name="author" type="text" value="' .
	                esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' class="form-control" /></div>' .
	                '<!-- #form-section-author .form-section -->',

	    'email'  => '<div class="form-group"><label for="email">' . __( 'Your Email:' ) . '</label> ' .
	                ( $req ? '<span class="required">*</span>' : '' ) .
	                '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' class="form-control" /></div>' .
	        		'<!-- #form-section-email .form-section -->',

	    'url'    => '' ) ),

	    'comment_notes_after' => '',
    );
}







?>