<?php
/**
 * The main template file.
 *
 * @package flatsome
 */

get_header();

?>
<div class="page-wrapper">
	<div class="row">
<div id="content" class="col-lg-9 left columns" role="main">
		<div class="page-inner">

		<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
				?>

			<?php endwhile; ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'index' ); ?>

		<?php endif; ?>

	</div><!-- .page-inner -->


	</div><!-- #content -->


	<div class="col-lg-3 columns left">
		<?php if($flatsome_opt['blog_layout'] == 'left-sidebar' || $flatsome_opt['blog_layout'] == 'right-sidebar'){
			get_sidebar();
		}?>
	</div><!-- end sidebar -->

</div><!-- end row -->
</div><!-- end page-wrapper -->


<?php get_footer(); ?>
