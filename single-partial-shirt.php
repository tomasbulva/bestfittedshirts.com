<?php
/**
 * The template for displaying single shirt
 *
 * @package WordPress
 * @subpackage Tomasome
 * @since now
 */


$coverImage = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' );
$galleryImages = get_field('gallery');

$price = get_field('price', get_the_ID());
$fits = get_the_category(get_the_ID());

$addtional_info = get_field('addtional_info', get_the_ID());
$review = get_field('review', get_the_ID());

$fitsLastName = end($fits)->name;
$wbuy = get_field('where_to_buy_url', get_the_ID());
$stars = get_field('stars', get_the_ID()); //strlen()
$picstars = "";
$postlink = get_permalink(get_the_ID());

for ($i2=0;$i2<5;$i2++){
    $picstars .= ($i2 < $stars) ? "<i class='glyphicon glyphicon-star'></i>" : "<i class='glyphicon glyphicon-star-empty'></i>";
}

?>
<div class="container">
    <div itemscope="" itemtype="http://schema.org/Product" id="product-<?php the_ID(); ?>" class="post-<?php the_ID(); ?>">
        <div class="row">
            <div class="col-md-6 col-sx-12 product-gallery">
                <div class="images">
                    <div class="product-image">
                        <div class="product-gallery-slider ux-slider slider-nav-circle-hover slider-nav-small  js-flickity flickity-enabled is-draggable" data-flickity-options="{ 
				            &quot;cellAlign&quot;: &quot;center&quot;,
				            &quot;wrapAround&quot;: true,
				            &quot;autoPlay&quot;: false,
				            &quot;prevNextButtons&quot;:true,
				            &quot;percentPosition&quot;: true,
				            &quot;imagesLoaded&quot;: true,
				            &quot;lazyLoad&quot;: 1,
				            &quot;pageDots&quot;: false,
				            &quot;selectedAttraction&quot; : 0.1,
				            &quot;friction&quot;: 0.6,
				            &quot;rightToLeft&quot;: false
				        }" tabindex="0">
                            <div class="slide first">
                                <img src="<?php echo $coverImage[0]; ?>" width="455" class="attachment-shop_single wp-post-image" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                            </div>

                            <?php if( $galleryImages ): 
                                foreach( $galleryImages as $image ): ?>
                                    <div class="slide">
                                        <img src="<?php echo $image['url']; ?>" width="455" class="attachment-shop_single wp-post-image" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- .product-image -->
                    
                    <ul class="product-thumbnails slider-no-arrows thumbnails js-flickity slider-nav-small flickity-enabled is-draggable" data-flickity-options="{ 
			            &quot;cellAlign&quot;: &quot;left&quot;,
			            &quot;wrapAround&quot;: false,
			            &quot;autoPlay&quot;: false,
			            &quot;prevNextButtons&quot;:true,
			            &quot;asNavFor&quot;: &quot;.product-gallery-slider&quot;,
			            &quot;percentPosition&quot;: true,
			            &quot;imagesLoaded&quot;: true,
			            &quot;pageDots&quot;: false,
			            &quot;selectedAttraction&quot; : 0.1,
			            &quot;friction&quot;: 0.6,
			            &quot;rightToLeft&quot;: false,
			            &quot;contain&quot;: true
			        }" tabindex="0">
                        <li class="first">
                            <img width="114"  src="<?php echo $coverImage[0]; ?>" class="attachment-shop_single wp-post-image" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" data-rel="prettyPhoto[product-gallery]">
                        </li>
                        <?php if( $galleryImages ): 
                            foreach( $galleryImages as $image ): ?>
                                <li>
                                    <img width="114" src="<?php echo $image['sizes']['thumbnail']; ?>" class="attachment-shop_single wp-post-image" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" data-rel="prettyPhoto[product-gallery]">
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
                <!-- .images -->
            </div>
            <!-- end large-6 - product-gallery -->

            <div class="product-info col-md-6 col-sx-12 left">
                <div class="col-sx-12">
                        <?php 
                            if ( function_exists('yoast_breadcrumb') ) {
                                echo bootstrapStyleBreadCrumbs(yoast_breadcrumb('','',false));
                            } 
                        ?>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <h1 itemprop="name" class="entry-title"><?php the_title(); ?></h1>
                        <div class="tx-div small"></div>
                        <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                            <p class="price"><span class="amount">$<?php echo number_format($price,2,".",","); ?></span></p>
                        </div>
                        <div class="tip-top tooltipstered">
                            <p><a href="#tab-reviews" class="scroll-to-reviews star-rating"><?php echo $picstars; ?></a></p>
                        </div>
                        <div itemprop="description" class="short-description">
                            <?php the_excerpt(); ?>
                            <p>&nbsp;</p>
                        </div>
                        <!-- goto retailer website BUTTON -->
                        <?php if($wbuy){ ?>
                            <a href="<?php echo $wbuy; ?>" title="Buy Here" class="btn btn-primary">Buy Here</a>
                        <?php } ?>
                        <div class="product_meta">
                            <span class="posted_in">
                                Fits: 
                                <?php
                                    foreach($fits as $fit){
                                        echo ($fit->name == $fitsLastName) ? $fit->name . "\n" : $fit->name . ", \n";
                                    }
                                ?>
                            </span>
                        </div>
                        <div class="tx-div small"></div>
                        <div class="social-icons share-row">
                            <!-- <a href="whatsapp://send?text='<?php echo urlencode ( get_the_title() ) ?>' - <?php echo $postlink ?>" data-action="share/whatsapp/share" class="icon icon_whatsapp tip-top tooltipstered" rel="nofollow"><i class="fa fa-whatsapp"></i></a>  -->
                            <a href="http://www.facebook.com/sharer.php?u=<?php echo $postlink ?>" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon icon_facebook tip-top tooltipstered"><i class="fa fa-facebook"></i></a> 
                            <a href="https://twitter.com/share?url=<?php echo $postlink ?>" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon icon_twitter tip-top tooltipstered"><i class="fa fa-twitter"></i></a> 
                            <a href="mailto:enteryour@addresshere.com?subject=<?php echo urlencode ( get_the_title() ) ?>&amp;body=Check%20this%20out:%20<?php echo $postlink ?>" rel="nofollow" class="icon icon_email tip-top tooltipstered"><i class="fa fa-envelope-o"></i></a> 
                            <a href="//pinterest.com/pin/create/button/?url=<?php echo $postlink ?>&amp;media=<?php echo $coverImage[0]; ?>&amp;description=<?php echo urlencode ( get_the_title() ) ?>" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon icon_pintrest tip-top tooltipstered"><i class="fa fa-pinterest"></i></a> 
                            <a href="//plus.google.com/share?url=<?php echo $postlink ?>" target="_blank" class="icon icon_googleplus tip-top tooltipstered" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow"><i class="fa fa-google-plus"></i></a> 
                            <a href="//tumblr.com/widgets/share/tool?canonicalUrl=<?php echo $postlink ?>" target="_blank" class="icon icon_tumblr tip-top tooltipstered" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow"><i class="fa fa-tumblr"></i></a> 
                        </div>
                        <div class="tx-div small"></div>
                        <?php echo get_simple_likes_button( get_the_ID() ); ?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo get_simple_comments_button( get_the_ID() ); ?>
                    </div>
                    <div class="product-page-aside col-md-4 text-center hide-for-small">
                        <!-- Back and forth between posts -->
                        
                            <div class="next-prev-nav">
                                <div class="prod-dropdown"><?php previous_post_link('%link','<i class="fa fa-angle-left"></i>'); ?></div>
                                <div class="prod-dropdown"><?php next_post_link('%link','<i class="fa fa-angle-right"></i>'); ?></div>
                            </div>
                        
                        <!-- .product-page-aside -->
                    </div>
                </div>
            </div>


        </div>
                <div class="product-details tabs-style">
                            <div class="tabbed-content wc-tabs-wrapper woocommerce-tabs">
                                <ul class="tabs wc-tabs nav-tabs" role="tablist">
                                    <?php 
                                    if($addtional_info || $review || ( comments_open(get_the_ID()) || '0' != get_comments_number(get_the_ID()) )){ 
                                    ?>
                                    <li class="description_tab active" role="presentation">
                                        <a href="#tab-description" aria-controls="profile" role="tab" data-toggle="tab">Description</a>
                                    </li>
                                    <?php 
                                    }
                                    if($addtional_info){ 
                                    ?>
                                    <li class="additional_information_tab" role="presentation">
                                        <a href="#tab-additional_information" aria-controls="profile" role="tab" data-toggle="tab">Additional Information</a>
                                    </li>
                                    <?php 
                                    } 
                                    if($review){
                                    ?>
                                    <li class="reviews_tab" role="presentation">
                                        <a href="#tab-reviews" aria-controls="profile" role="tab" data-toggle="tab">Reviews</a>
                                    </li>
                                    <?php
                                    }
                                    if ( comments_open(get_the_ID()) || '0' != get_comments_number(get_the_ID()) ){
                                    ?>
                                    <li class="comments_tab" role="presentation">
                                        <a href="#respond" aria-controls="profile" role="tab" data-toggle="tab">Comments <?php echo ( '0' != get_comments_number(get_the_ID()) )? "(" . get_comments_number(get_the_ID()) . ")" : ""; ?></a>
                                    </li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane panel wc-tab entry-content active" id="tab-description" role="tabpanel" >
                                        <div id="description">
                                        <?php 
                                            global $more;
                                            $more = 1;
                                            the_content();
                                        ?>
                                        </div><!--  #description -->
                                    </div>
                                    <?php if($addtional_info){ ?>
                                    <div class="tab-pane panel wc-tab entry-content" id="tab-additional_information" role="tabpanel" >
                                        <div id="addtional_info">
                                            <?php echo $addtional_info; ?>
                                        </div><!--  #addtional_info -->
                                    </div>
                                    <?php 
                                    } 
                                    if($review){
                                    ?>
                                    <div class="tab-pane panel wc-tab entry-content" id="tab-reviews" role="tabpanel" >
                                        <div id="reviews">
                                            <?php echo $review; ?>
                                        </div><!-- #reviews -->
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    <div class="tab-pane panel wc-tab entry-content" id="respond" role="tabpanel" >
                                        <div id="comments">
                                            <?php 
                                                // If comments are open or we have at least one comment, load up the comment template
                                                if ( comments_open(get_the_ID()) || '0' != get_comments_number(get_the_ID()) ){
                                                    comments_template(get_the_ID());
                                                }else{
                                                    echo "comment form placeholder";
                                                }
                                            ?>
                                        </div><!-- #reviews -->
                                    </div>
                                </div>
                            </div>
                            <!-- .tabbed-content -->
                </div>
    </div>
    <!-- #product-<?php the_ID(); ?> -->
</div> <!-- end .container -->