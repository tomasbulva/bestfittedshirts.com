<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' );?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">

	<header id="masthead" class="site-header" role="banner">
		<div class="container">
			<nav class="navbar navbar-default margintop">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" style="line-height:50px; font-size:20px;" title="Home">
			      	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.png"> 
			      </a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse rightmenu" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav navbar-right">
			       <?php
			            $main_menu_right = array(
							'theme_location'    => 'primary-nav',
							'menu_class'        => 'nav navbar-nav navbar-right',
							'echo'              =>  true,
							'container'         => 'span',
							'container_class'   => '',
							'container_id'      => '',
							'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
							'walker'            =>  new wp_bootstrap_navwalker(),
			            );
			            wp_nav_menu( $main_menu_right );
			        ?>
			      </ul>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</header><!-- .site-header -->


	

	<div id="content" class="site-content">
