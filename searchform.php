<?php
/**
 * The template for displaying search forms in flatsome
 *
 * @package flatsome
 */
?>


<div class="row search-wrapper">
	<form method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	  	<div class="col-lg-12 col-sx-12">
			<div class="input-group">
				<input type="search" class="form-control" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php echo _e( 'Search', 'woocommerce' ); ?>&hellip;" />
				<span class="input-group-btn">
					<button class="btn btn-danger" type="button"><i class="fa fa-search"></i></button>
				</span>
			</div><!-- /input-group -->
		</div><!-- button -->
	</form>
</div><!-- row -->

