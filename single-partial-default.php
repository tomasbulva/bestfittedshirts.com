<div id="primary" class="content-area">
		<main id="main" class="site-main blog row single-post page-right-sidebar" role="main">
			<div class="container">
				<div class="<?php if ( is_active_sidebar( 'sidebar-main' ) ) : ?>col-sm-8 col-md-8 col-lg-8<?php endif; ?> col-sx-12 page-inner">
					<?php if ( have_posts() ) : ?>

						<?php
						// Start the loop.
						while ( have_posts() ) : the_post();
							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
						?>
						<div class="col-sx-12">
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<header class="entry-header text-center">
										<?php
										if ( is_single() ) :
											the_title( '<h1 class="entry-title">', '</h1>' );
										else :
											the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
										endif;
										?>
										<div class="tx-div small center"></div>
										<?php if ( 'post' == get_post_type() ) : ?>
										<div class="entry-meta">
											<?php flatsome_posted_on(); ?>
										</div><!-- .entry-meta -->
										<?php endif; ?>
									</header><!-- .entry-header -->
									
									<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?>
								        <div class="entry-image">
									    	<?php the_post_thumbnail('full'); ?>

								            <div class="post-date large">
									                <span class="post-date-day"><?php echo get_the_time('d', get_the_ID()); ?></span>
									                <span class="post-date-month"><?php echo get_the_time('M', get_the_ID()); ?></span>
								            </div>
								        </div>
								    <?php } ?>

								<div class="entry-content">
								<?php
									/* translators: %s: Name of current post */
									the_content( sprintf(
										__( 'Continue reading %s', 'twentyfifteen' ),
										the_title( '<span class="screen-reader-text">', '</span>', false )
									) );

									wp_link_pages( array(
										'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
										'after'       => '</div>',
										'link_before' => '<span>',
										'link_after'  => '</span>',
										'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
										'separator'   => '<span class="screen-reader-text">, </span>',
									) );
								?>
								</div><!-- .entry-content -->

								<?php
									// Author bio.
									if ( is_single() && get_the_author_meta( 'description' ) ) :
								?>
										<div class="author-info">
											<h2 class="author-heading"><?php _e( 'Published by', 'twentyfifteen' ); ?></h2>
											<div class="author-avatar">
												<?php
												/**
												 * Filter the author bio avatar size.
												 *
												 * @since Twenty Fifteen 1.0
												 *
												 * @param int $size The avatar height and width size in pixels.
												 */
												$author_bio_avatar_size = apply_filters( 'twentyfifteen_author_bio_avatar_size', 56 );

												echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
												?>
											</div><!-- .author-avatar -->

											<div class="author-description">
												<h3 class="author-title"><?php echo get_the_author(); ?></h3>

												<p class="author-bio">
													<?php the_author_meta( 'description' ); ?>
													<a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
														<?php printf( __( 'View all posts by %s', 'twentyfifteen' ), get_the_author() ); ?>
													</a>
												</p><!-- .author-bio -->

											</div><!-- .author-description -->
										</div><!-- .author-info -->
								<?php
									endif; // Author bio. end
								?>

								<footer class="entry-footer">
									<?php my_entry_meta(); ?>
									<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
								</footer><!-- .entry-footer -->

							</article><!-- #post-## -->
							<?php
								// If comments are open or we have at least one comment, load up the comment template.
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;
								// Previous/next post navigation.
								the_post_navigation( array(
									'next_text' => '<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
										'<span class="post-title">%title</span>',
									'prev_text' => '<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
										'<span class="post-title">%title</span>',
								) );

							// End the loop.
							endwhile;
							endif;
							?>
					</div>
				</div>
				<?php if ( is_active_sidebar( 'sidebar-main' ) ) : ?>
				<div class="col-sm-4 col-md-4 col-lg-4 col-sx-12">
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'sidebar-main' ); ?>
						</div><!-- #primary-sidebar -->
				</div>
				<?php endif; ?>
			</div> <!-- .container -->
		</main><!-- .site-main -->
	</div><!-- .content-area -->