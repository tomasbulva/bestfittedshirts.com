/* global screenReaderText */
/**
 * Theme functions file.
 *
 * Contains handlers for navigation and widget area.
 */


( function( $ ) {

	window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';
	
	
	
	switch(true){
		case ($('.deal_of_the_week_box').length > 0):
			var wayEle = $('.deal_of_the_week_box')[0];
		break;
		case ($('.post').length > 0):
			var wayEle = $('.post')[0];
		break;
		case ($('.product-info').length > 0):
			var wayEle = $('.product-info')[0];
		break;
		case ($('.post-thumbnail').length > 0):
			var wayEle = $('.post-thumbnail')[0];
		break;
		default:
			var wayEle = $('.carousel-inner')[0];
	}

	var waypoint = new Waypoint({
	  element: wayEle,
	  handler: function(direction) {
	    console.log(this.id + ' hit')
	    if(direction == 'down'){
	    	$('#masthead').addClass('stuck');
	    	$('.site-content').addClass('headerstuck');
	    }else{
	    	$('#masthead').removeClass('stuck');
	    	$('.site-content').removeClass('headerstuck');
	    }
	  }
	})

	$(document).on('click', '.sl-button', function() {
		var button = $(this);
		var post_id = button.attr('data-post-id');
		var security = button.attr('data-nonce');
		var iscomment = button.attr('data-iscomment');
		var allbuttons;
		if ( iscomment === '1' ) { /* Comments can have same id */
			allbuttons = $('.sl-comment-button-'+post_id);
		} else {
			allbuttons = $('.sl-button-'+post_id);
		}
		var loader = allbuttons.next('#sl-loader');
		if (post_id !== '') {
			$.ajax({
				type: 'POST',
				url: simpleLikes.ajaxurl,
				data : {
					action : 'process_simple_like',
					post_id : post_id,
					nonce : security,
					is_comment : iscomment
				},
				beforeSend:function(){
					loader.html('&nbsp;<div class="loader">Loading...</div>');
				},	
				success: function(response){
					var icon = response.icon;
					var count = response.count;
					allbuttons.html(icon+count);
					if(response.status === 'unliked') {
						var like_text = simpleLikes.like;
						allbuttons.prop('title', like_text);
						allbuttons.removeClass('liked');
					} else {
						var unlike_text = simpleLikes.unlike;
						allbuttons.prop('title', unlike_text);
						allbuttons.addClass('liked');
					}
					loader.empty();					
				}
			});
			
		}
		return false;
	});

	// Javascript to enable link to tab
	var url = document.location.toString();
	if (url.match('#')) {
	    $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
	}

	// Change hash for page-reload
	$('.nav-tabs a').on('shown.bs.tab', function (e) {
	    window.location.hash = e.target.hash;
	})

}( jQuery ) );
var $mcj = jQuery.noConflict(true);