=== Best Fitted Shirts ===
Creator: Tomas Bulva
Tags: Custom
Requires at least: 4.1
Tested up to: 4.4
Stable tag: 4.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Custom Template based on combination of flatsome and twantyfifteen templates

* Responsive Layout
* Custom Colors
* Custom Header
* Social Links
* Menu Description
* Post Formats
* The GPL v2.0 or later license. :) Use it to make something cool.