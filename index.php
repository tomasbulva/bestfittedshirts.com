<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main blog row page-right-sidebar" role="main">
			<div class="container">
				<div class="<?php if ( is_active_sidebar( 'sidebar-main' ) ) : ?>col-sm-8 col-md-8 col-lg-8<?php endif; ?> col-sx-12 page-inner">
					<?php if ( have_posts() ) : ?>

						<?php
						// Start the loop.
						while ( have_posts() ) : the_post();
							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							
							$flatsome_opt['blog_style'] = 'blog-normal';

							// BLOG NORMAL STYLE
						?>
							<div class="col-sx-12">
								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<header class="entry-header text-center">
										<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
										<div class="tx-div small center"></div>
										<?php if ( 'post' == get_post_type() ) : ?>
										<div class="entry-meta">
											<?php flatsome_posted_on(); ?>
										</div><!-- .entry-meta -->
										<?php endif; ?>
									</header><!-- .entry-header -->


									<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?>
								        <div class="entry-image">
								        	<a href="<?php the_permalink();?>">
									            <?php the_post_thumbnail('full'); ?>
								        	</a>

								            <div class="post-date large">
									                <span class="post-date-day"><?php echo get_the_time('d', get_the_ID()); ?></span>
									                <span class="post-date-month"><?php echo get_the_time('M', get_the_ID()); ?></span>
								            </div>
								        </div>
								    <?php } ?>

									<?php if ( is_search() ) : // Only display Excerpts for Search ?>
									<div class="entry-summary">
										<?php the_excerpt(); ?>
									</div><!-- .entry-summary -->
									<?php else : ?>
									<div class="entry-content">
										<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'flatsome' ) ); ?>
										<?php
											wp_link_pages( array(
												'before' => '<div class="page-links">' . __( 'Pages:', 'flatsome' ),
												'after'  => '</div>',
											) );
										?>
									</div><!-- .entry-content -->
									<?php endif; ?>

									<articlefooter class="entry-meta">
										<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
											<?php
												/* translators: used between list items, there is a space after the comma */
												$categories_list = get_the_category_list( __( ', ', 'flatsome' ) );
											?>
											<span class="cat-links">
												<?php printf( __( 'Posted in %1$s', 'flatsome' ), $categories_list ); ?>
											</span>

											<?php
												/* translators: used between list items, there is a space after the comma */
												$tags_list = get_the_tag_list( '', __( ', ', 'flatsome' ) );
												if ( $tags_list ) :
											?>
											<span class="sep"> | </span>
											<span class="tags-links">
												<?php printf( __( 'Tagged %1$s', 'flatsome' ), $tags_list ); ?>
											</span>
											<?php endif; // End if $tags_list ?>
										<?php endif; // End if 'post' == get_post_type() ?>

										<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
										<span class="comments-link right"><?php comments_popup_link( __( 'Leave a comment', 'flatsome' ), __( '<strong>1</strong> Comment', 'flatsome' ), __( '<strong>%</strong> Comments', 'flatsome' ) ); ?></span>
										<?php endif; ?>
									</articlefooter><!-- .entry-meta -->
								</article><!-- #post-## -->
							</div>

						<?php
						// End the loop.
						endwhile;

						// Previous/next page navigation.
						the_posts_pagination( array(
							'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
							'next_text'          => __( 'Next page', 'twentyfifteen' ),
							'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
						) );

						// If no content, include the "No posts found" template.
						else :
							get_template_part( 'content', 'none' );
						endif;
					?>
				</div>
				<?php if ( is_active_sidebar( 'sidebar-main' ) ) : ?>
				<div class="col-sm-4 col-md-4 col-lg-4 col-sx-12">
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php dynamic_sidebar( 'sidebar-main' ); ?>
						</div><!-- #primary-sidebar -->
				</div>
				<?php endif; ?>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
