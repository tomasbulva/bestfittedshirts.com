<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); 

if(!$wp_query) global $wp_query;
$post_type = $wp_query->query_vars['post_type'];

//print_r($post_type);

switch($post_type){
	case "shirt":
		get_template_part('single-partial-shirt');
		break;
	default:
		get_template_part('single-partial-default');
		break;
}

?>

<?php get_footer(); ?>
